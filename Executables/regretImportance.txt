Issues=10
PrototypesJump=10
Runs=100
Voters=1000
Parties=10
ElectoralThreshold=0.05
OutputDirectory=RegretImportance
BetaValues=1,1|2,2|3,3|5,5|10,10

BaseNoisePrototypes=10
BaseNoiseVoters=10
NoisePrototypes=0,1,2,3,4,5
NoiseVoters=0,1,2,3,4,5
ImportantIssues=10,30,50,70,90
