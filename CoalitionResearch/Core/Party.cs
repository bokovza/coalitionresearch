﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CoalitionResearch.Core.Profiles;

namespace CoalitionResearch.Core
{
    /// <summary>
    /// Defines a party of voters
    /// </summary>
    public class Party
    {
        #region Members
        /// <summary>
        /// Voters
        /// </summary>
        private readonly IDictionary<int, Voter> _voters = new Dictionary<int, Voter>();

        #endregion

        #region Properties
        /// <summary>
        /// Public Voters Dictionary
        /// </summary>
        public IReadOnlyDictionary<int, Voter> Voters { get; }

        /// <summary>
        /// Party Size - amount of voters with party
        /// </summary>
        public int Size => Voters.Count;

        /// <summary>
        /// The Profile of the party
        /// </summary>
        public PartyProfile Profile { get; }

        /// <summary>
        /// The header of the profile - shortcut
        /// </summary>
        public ProfileHeader Header => Context.Header;
        /// <summary>
        /// The Voting Context
        /// </summary>
        public VotingContext Context { get; }

        /// <summary>
        /// The Party's Id
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Shortcut to the Profile's Data
        /// </summary>
        /// <param name="index">Index</param>
        /// <returns>Data[Index]</returns>
        public int this[int index]
        {
            get { return Profile[index]; }
            set { Profile[index] = value; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Basic Constructor
        /// </summary>
        /// <param name="context">The Voting Context</param>
        /// <param name="id">Id of Party</param>
        public Party(VotingContext context, int id)
        {
            if ((Context = context) == null)
                throw new ArgumentNullException(nameof(Context));

            Profile = new PartyProfile(this);
            Voters = new ReadOnlyDictionary<int, Voter>(_voters);
            Id = id;
        }
        #endregion

        #region Methods
        /// <summary>
        /// This method set the profile of the party based on the voter who is given as a parameter.
        /// </summary>
        /// <param name="v">The voter</param>
        public void SetFirstVoter(Voter v)
        {
            if (v == null)
                throw new ArgumentNullException(nameof(v));

            v.Party = this;
            Copy(v);
        }


        public void Copy(Voter v) => Profile.Copy(v?.Profile);

        /// <summary>
        /// Shortcut for Stablize in the profile
        /// </summary>
        public void Stablize() => Profile.Stablize();

        #endregion

        #region Coalition Formatting

        /// <summary>
        /// Calculates the utility of this party given a coalition c
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// Adds a voter to the party
        /// </summary>
        /// <param name="v">Voter to add</param>
        public void Add(Voter v)
        {
            if (v != null)
                _voters.Add(v.Id, v);
        }
        /// <summary>
        /// Removes a voter from a party
        /// </summary>
        /// <param name="v"></param>
        public void Remove(Voter v)
        {
            if (v != null)
                Remove(v.Id);
        }
        /// <summary>
        /// Removes a voter from a party
        /// </summary>
        /// <param name="id">The id of the voter</param>
        public void Remove(int id) => _voters.Remove(id);
        #endregion

        #region Overrides
        public override string ToString() => $"{Id} => {Profile}";
        #endregion
    }
}