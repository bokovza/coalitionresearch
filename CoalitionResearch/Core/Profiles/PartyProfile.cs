﻿using System;
using System.Linq;

namespace CoalitionResearch.Core.Profiles
{
    /// <summary>
    /// Profile for a party
    /// </summary>
    public class PartyProfile : PowerProfile
    {
        #region Properties
        /// <summary>
        /// The party which this profile is belong to.
        /// </summary>
        public Party Party { get; }

        #endregion

        #region Constructors
        /// <summary>
        /// Basic Constructor
        /// </summary>
        /// <param name="p">The party</param>
        public PartyProfile(Party p) : base(p?.Header)
        {
            if ((Party = p) == null)
                throw new ArgumentNullException(nameof(Party));
        }

        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="p">The profile to copy from</param>
        public PartyProfile(PartyProfile p) : base(p)
        {
            Party = p.Party;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Updates the whole profile based on the voters that are listed to the party.
        /// </summary>
        public void Stablize()
        {
            for (var i = 0; i < Size; i++)
            {
                var sum = Party.Voters.Values.Sum(voter => voter.Profile[i]);
                this[i] = sum >= 0 ? 1 : -1;
                Power[i] = Math.Abs(sum);
            }
        }

        /// <summary>
        /// Copy the values of the given profile to this profile
        /// </summary>
        /// <param name="p">Source Profile</param>
        /// <returns>True if the method executed successfully</returns>
        public override bool Copy(Profile p)
        {
            if (p == null || !Equals(p.Header, Header))
                return false;

            for (var i = 0; i < Size; i++)
                this[i] = p[i];

            return true;
        }
        #endregion
    }
}