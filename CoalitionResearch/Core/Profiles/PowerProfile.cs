﻿using System.Collections.Generic;
using CoalitionResearch.Core.Utilities;

namespace CoalitionResearch.Core.Profiles
{
    /// <summary>
    /// A profile, which each issue have also a weight next to the value.
    /// </summary>
    public abstract class PowerProfile : Profile
    {
        #region Properties
        /// <summary>
        /// The power of the profile
        /// </summary>
        public int[] Power { get; }
        #endregion

        #region Constructors
        /// <summary>
        /// Basic Constructors
        /// </summary>
        /// <param name="header">The Profile Header of the profile</param>
        protected PowerProfile(ProfileHeader header) : base(header)
        {
            // ReSharper disable once VirtualMemberCallInContructor
            Power = new int[Size];
        }

        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="p">The profile we copy from</param>
        protected PowerProfile(PowerProfile p) : base(p)
        {
            Power = new int[Size];
            for (var i = 0; i < Size; i++)
                Power[i] = p.Power[i];
        }
        #endregion

        #region Overrides
        public override string ToString()
        {
            var lst = new List<object>();
            for(var i = 0; i < Header.Size; i++)
                lst.Add($"<{this[i]},{Power[i]}>");

            return $"[{StringFunctions.CreateListString(lst, ';')}]";
        }
        #endregion
    }
}