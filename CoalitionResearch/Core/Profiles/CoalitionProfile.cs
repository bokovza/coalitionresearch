﻿using System;
using System.Linq;

namespace CoalitionResearch.Core.Profiles
{
    /// <summary>
    /// Profile for a coalition
    /// </summary>
    public class CoalitionProfile : PowerProfile
    {
        #region Properties
        /// <summary>
        /// The coalition that the profile is belong to
        /// </summary>
        public Coalition Coalition { get; }
        #endregion

        #region Constructors
        /// <summary>
        /// Basic Constructor
        /// </summary>
        /// <param name="c">A Coalition</param>
        public CoalitionProfile(Coalition c) : base(c?.Context.Header)
        {
            if((Coalition = c) == null)
                throw new ArgumentNullException(nameof(c));

            Stablize();
        }

        /// <summary>
        /// Calculates the profile based on the parties that combines the coalition.
        /// </summary>
        private void Stablize()
        {
            var length = Coalition.Context.Header.Size;
            var power = Coalition.Size;
            for (var i = 0; i < length; i++)
            {
                Power[i] = power;
                this[i] = Coalition.Parties.Sum(party => party.Profile[i]*party.Profile.Power[i]) > 0 ? 1 : -1;
            }
        }
        #endregion
    }
}