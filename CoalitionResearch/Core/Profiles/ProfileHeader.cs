﻿using System;

namespace CoalitionResearch.Core.Profiles
{
    /// <summary>
    /// The meta data about the profile in the voting system
    /// </summary>
    public class ProfileHeader
    {
        #region Properties
        /// <summary>
        /// Amount of issues
        /// </summary>
        public int Size { get; }
        #endregion

        #region Constructors
        /// <summary>
        /// Basic Constructor
        /// </summary>
        /// <param name="size">Amount of issues</param>
        public ProfileHeader(int size /*, params int[] important*/)
        {
            if ((Size = size) < 1)
                throw new ArgumentOutOfRangeException(nameof(size), "Size of profile must be positive and non-zero.");
        }
        #endregion

        #region Overrrides

        public override bool Equals(object obj)
        {
            var h = obj as ProfileHeader;
            return (h?.Size ?? -1) == Size;
        }

        protected bool Equals(ProfileHeader other)
        {
            return Size == other.Size;
        }

        public override int GetHashCode()
        {
            return Size;
        }

        #endregion
    }
}