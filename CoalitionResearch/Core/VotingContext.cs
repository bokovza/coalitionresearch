﻿using System;
using System.Collections.Generic;
using CoalitionResearch.Core.Profiles;
using System.Linq;
using CoalitionResearch.Core.Utilities;

namespace CoalitionResearch.Core
{
    /// <summary>
    /// The Context that contain all the voters and parties in the world
    /// </summary>
    public class VotingContext
    {
        #region Members
        /// <summary>
        /// Set of all Parties
        /// </summary>
        public ISet<Party> Parties { get; private set; }

        /// <summary>
        /// Set of all Voters
        /// </summary>
        public Voter[] Voters { get; private set; }
        #endregion

        #region Properties
        /// <summary>
        /// The Header of the Profile
        /// </summary>
        public ProfileHeader Header { get; }
        /// Counter to give Id numbers for the parties
        /// </summary>
        public Counter CounterParties { get; } = new Counter();
        /// <summary>
        /// Counter to give Id numbers for the voters
        /// </summary>
        public Counter CounterVoters { get; } = new Counter();
        /// <summary>
        /// The Electoral Threshold (precentages)
        /// </summary>
        public double ElectoralThreshold { get; set; }
        /// <summary>
        /// Number of voters to reach Electoral Threshold
        /// </summary>
        public int ElectoralThresholdVotersSize => (int)(Math.Round(ElectoralThreshold * Voters.Length)) + 1;
        /// <summary>
        /// Number of voters to reach Majority of voters
        /// </summary>
        public int MajorityVotersSize => (int)Math.Round(0.5 * Voters.Length) + 1;
        /// <summary>
        /// List of Proper Coalitions
        /// </summary>
        public ICollection<Coalition> ProperCoalitions { get; }
        /// <summary>
        /// List of Coalitions in the core
        /// </summary>
        public ICollection<Coalition> CoreCoalitions { get; }
        
        /// <summary>
        /// The Profile that presents the majority of voters' opinion.
        /// </summary>
        public Profile Majority { get; private set; }

        //public double SinglePartySingleVoterUtility => SinglePartyValue / Voters.Length;
        public double SinglePartyValue { get; private set; } = double.NaN;

        /// <summary>
        /// Amount of voters in the context
        /// </summary>
        private int VotersCount { get; }
        /// <summary>
        /// Amount of parties in the context
        /// </summary>
        private int PartiesCount { get; }
        #endregion

        #region Constructors

        /// <summary>
        /// Basic Constructor
        /// </summary>
        /// <param name="header">Profile's Header</param>
        /// <param name="voters">Amount of voters at startup</param>
        /// <param name="parties">Amount of parties at startup</param>
        public VotingContext(ProfileHeader header, int voters, int parties)
        {
            if ((Header = header) == null)
                throw new ArgumentNullException(nameof(header));
            if ((VotersCount = voters) <= 0)
                throw new ArgumentOutOfRangeException(nameof(voters), "Must be above 0.");
            if ((PartiesCount = parties) <= 0)
                throw new ArgumentOutOfRangeException(nameof(parties), "Must be above 0.");

            Parties = new HashSet<Party>();
            Voters = new Voter[VotersCount];

            ProperCoalitions = new List<Coalition>();
            CoreCoalitions = new List<Coalition>();
            
            Majority = new Profile(Header);

            Build();
            Reset();
        }

        /// <summary>
        /// Build the voters of the context.
        /// </summary>
        private void Build()
        {
            for (int i = 0, j = 1; i < VotersCount; i++, j++)
                Voters[i] = new Voter(this, j);
        }

        /// <summary>
        /// Reset the context from scratch.
        /// </summary>
        public void Reset()
        {
            foreach (var voter in Voters)
                voter.Party = null;

            Parties.Clear();
            for (var i = 1; i <= PartiesCount; i++)
                Parties.Add(new Party(this, i));

            SinglePartyValue = double.NaN;
        }
        #endregion

        #region Methods
        /// <summary>
        /// This method stablize the parties, and remove parties with voters under the electoral threshold.
        /// </summary>
        /// <returns>Amount of iterations</returns>
        /// TODO: Fix This &amp; Add Documentation
        public int Stablize()
        {
            var parties = Parties.ToList();
            var iterations = 0;

            var isChanged = true;

            while (isChanged)
            {
                var k = 0;
                foreach (var voter in Voters)
                    if (voter.FindParty(parties))
                        k++;

                var emptyParties = parties.Where(party => party.Size < ElectoralThresholdVotersSize).ToList();
                isChanged = emptyParties.Count > 0 || k > 0;
                parties = parties.Except(emptyParties).ToList();

                foreach (var party in emptyParties)
                {
                    var voters = party.Voters.Values.ToList();
                    foreach (var voter in voters)
                        voter.FindParty(parties);
                    Parties.Remove(party);
                }

                parties.ForEach(party => party.Stablize());

                if (isChanged)
                    iterations++;
            }

            return iterations;
        }

        /// <summary>
        /// Creates proper coalitions and put it in the ProperCoalitions' List
        /// </summary>
        private void GenerateProperCoalitions()
        {
            ProperCoalitions.Clear();

            var parties = Parties.ToList();
            var incrementor = new Incrementor(parties.Count);

            while (!incrementor.Overflow)
            {
                var cParties = new HashSet<Party>();
                for (var i = 0; i < parties.Count; i++)
                    if (incrementor[i])
                        cParties.Add(parties[i]);

                var coalition = new Coalition(this, cParties);
                if (coalition.IsProper)
                    ProperCoalitions.Add(new Coalition(this, cParties));

                incrementor.Increment();
            }
        }

        /// <summary>
        /// Creates coalitions of the core and put it in the CoreCoalitions' List
        /// </summary>
        private void GenerateCoreCoalitions()
        {
            GenerateProperCoalitions();

            CoreCoalitions.Clear();
            if (ProperCoalitions.Count <= 1)
            {
                if (ProperCoalitions.Count == 1)
                    CoreCoalitions.Add(ProperCoalitions.ElementAt(0));
                return;
            }

            foreach (var coalition in ProperCoalitions)
            {
                if (coalition == null)
                    continue;

                var inCore = true;

                foreach (var otherCoalition in ProperCoalitions)
                {
                    if (otherCoalition == coalition) continue;

                    if (
#if REGRET
                        otherCoalition.Parties.All(party => otherCoalition.Values[party] < coalition.Values[party])
#else
                        otherCoalition.Parties.All(party => otherCoalition.Values[party] > coalition.Values[party])
#endif 
                        
                        )
                    {
                        inCore = false;
                        break;
                    }
                }

                if (inCore)
                    CoreCoalitions.Add(coalition);
            }
        }

        /// <summary>
        /// Creates The profile of the Single Party - The Majority Party
        /// </summary>
        public void GenerateMajor()
        {
            for (var i = 0; i < Header.Size; i++)
                Majority[i] = Voters.Sum(voter => voter.Profile[i]) >= 0 ? 1 : -1;
        
            SinglePartyValue = GetSinglePartyValue(Voters);
        }

        /// <summary>
        /// Finds the best coalition, using priority based on parties size
        /// </summary>
        /// <returns>The best coalition</returns>
        public Coalition FormatCoalition(out int leaderIndex, out bool isMinimumCardinals)
        {
            var parties = Parties.OrderByDescending(party => party.Size).Select(item => item).ToList();
            GenerateCoreCoalitions();

            leaderIndex = -1;
            var count = 0;

            foreach (var party in parties)
            {
                var core = CoreCoalitions.Where(coalition => coalition.Parties.Contains(party)).ToList();
                if (core.Count == 0)
                {
                    count++;
                    continue;
                }

                var minSize = core.Min(coalition => coalition.Size);
                var maxCoalition = core.ElementAt(0);
                var maxValue = maxCoalition.Values[party];

                for (var i = 1; i < core.Count; i++)
                {
                    var coalition = core.ElementAt(i);
                    var utility = coalition.Values[party];

                    if (
#if REGRET
                        maxValue <= utility
#else
                        maxValue >= utility
#endif
                        ) continue;

                    maxCoalition = coalition;
                    maxValue = utility;
                }

                leaderIndex = count;
                isMinimumCardinals = minSize == maxCoalition.Size;

                return maxCoalition;
            }

            isMinimumCardinals = true;
            return null;
        }

        /// <summary>
        /// Gets a set of voters and returns the value of those voters if they were in the Single Party
        /// </summary>
        /// <param name="voters">List of Voters</param>
        /// <returns>Value of voters, if they were in the Single Party</returns>
        public double GetSinglePartyValue(ICollection<Voter> voters)
        {
            if ((voters?.Count ?? 0) <= 0)
                return 0;

            var result = 0.0;

            foreach(var voter in voters)
            {
#if REGRET
                    result += Majority.HammingDistance(voter.Profile);
#else
                    for(var i = 0; i < Majority.Size; i++)
                        result += Majority[i] * voter[i];
#endif
            }

            return result;
        }
        #endregion
    }
}