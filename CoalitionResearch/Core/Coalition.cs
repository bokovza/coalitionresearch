﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CoalitionResearch.Core.Profiles;
using CoalitionResearch.Core.Utilities;

namespace CoalitionResearch.Core
{
    /// <summary>
    /// A set of parties, which combines as a "single party"
    /// </summary>
    public class Coalition
    {
        #region Properties
        /// <summary>
        /// The Context of the Coalition
        /// </summary>
        public VotingContext Context { get; }
        /// <summary>
        /// The voters from all the parties, which are in the coalition
        /// </summary>
        public ICollection<Voter> Voters { get; }
        /// <summary>
        /// The parties in the coalition
        /// </summary>
        public ICollection<Party> Parties { get; }
        /// <summary>
        /// Coalition's Profile
        /// </summary>
        public CoalitionProfile Profile { get; }
        /// <summary>
        /// The resulted profile, given the coalition
        /// </summary>
        public Profile ResultedProfile { get; }
        /// <summary>
        /// Amount of voters in the coalition
        /// </summary>
        public int Size => Voters.Count;
        /// <summary>
        /// Returns true if the amount of voters in the coalition is bigger than 50% of all voters in the context.
        /// </summary>
        public bool IsProper => Size > Context.MajorityVotersSize;
        /// <summary>
        /// Shortcut for the value of an issue in the coalition's profile.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public int this[int index] => Profile[index];

        #region Value
        /// <summary>
        /// The Single Party Value of the Voters in this Coalition.
        /// </summary>
        public double CoalitionSinglePartyValue => Context.GetSinglePartyValue(Voters);

        /// <summary>
        /// The Single Party Value of the Voters that are not in this coalition.
        /// </summary>
        public double OppositionSinglePartyValue => Context.GetSinglePartyValue(Context.Voters.Except(Voters).ToList());
        #endregion

        /// <summary>
        /// Dictionary with the values of each party in the context
        /// </summary>
        private readonly IDictionary<Party, double> _values = new Dictionary<Party, double>();
        /// <summary>
        /// Public dictionary with the values of each party in the context - no editable.
        /// </summary>
        public IReadOnlyDictionary<Party, double> Values { get; }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context">The context of the coalition</param>
        /// <param name="parties">The list of parties that are in the coalition</param>
        public Coalition(VotingContext context, ICollection<Party> parties = null)
        {
            if ((Context = context) == null)
                throw new ArgumentNullException(nameof(Context));

            Parties = parties ?? new Party[0];
            Voters = Parties.SelectMany(party => party.Voters.Values).ToList();

            if (IsProper)
            {

                Profile = new CoalitionProfile(this);
                Values = new ReadOnlyDictionary<Party, double>(_values);

                ResultedProfile = CalculateResultedProfile();
                CalculateValues();
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Calculation of the Resulted Profile
        /// </summary>
        /// <returns>Resulted Profile</returns>
        private Profile CalculateResultedProfile()
        {
            var result = new Profile(Context.Header);

            var lst = new List<PowerProfile>() { Profile };
            lst.AddRange(Context.Parties.Except(Parties).Select(party => party.Profile).ToList());

            for (var i = 0; i < Context.Header.Size; i++)
                result[i] = lst.Sum(profile => profile[i] * profile.Power[i]) > 0 ? 1 : -1;

            return result;
        }



        /// <summary>
        /// Calculation of the values of all parties in the context.
        /// </summary>
        private void CalculateValues()
        {
            foreach (var party in Context.Parties)
            {
                var value = 0.0;
#if REGRET
                value = GetPartyRegret(party);
#else
                for (var i = 0; i < Context.Header.Size; i++)
                    value += ResultedProfile[i] * party[i] * party.Profile.Power[i];
#endif

                _values.Add(party, value);
            }
        }

        /// <summary>
        /// Calculates and return the Regret Value of a Party p
        /// </summary>
        /// <param name="p">A party</param>
        /// <returns>Regret(p,P/C)</returns>
        private double GetPartyRegret(Party p)
        {
            if ((p?.Voters.Count ?? 0) == 0)
                return 0;

            var result = 0.0;
            foreach (var voter in p.Voters.Values)
                result += voter.Profile.HammingDistance(ResultedProfile);

            return result;
        }

        #endregion
        
        #region Overrides
        public override string ToString() => $"[{StringFunctions.CreateListString(Parties.Select(party => party.Id), ' ')}]";
        #endregion
    }
}