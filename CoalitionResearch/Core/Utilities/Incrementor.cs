﻿using System;

namespace CoalitionResearch.Core.Utilities
{
    /// <summary>
    /// Used for the proper coalition algorithm
    /// Allow handle 2^n options
    /// </summary>
    public class Incrementor
    {
        #region Properties
        /// <summary>
        /// Used to know which parties will be in the coalition, and which parties are not in the coalition.
        /// </summary>
        private bool[] Data { get; }
        /// <summary>
        /// True when the algorithm is finished pass all the coalitions avaliable
        /// </summary>
        public bool Overflow { get; private set; }
        /// <summary>
        /// Amount of parties that avaliable
        /// </summary>
        public int Size => Data?.Length ?? 0;
        /// <summary>
        /// Mark[i] of the party - contain in the coalition, or not
        /// </summary>
        /// <param name="i">Index of a party</param>
        /// <returns></returns>
        public bool this[int i]
        {
            get { return Data[i]; }
            private set { Data[i] = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Basic Constructor
        /// </summary>
        /// <param name="size">Amount of Parties</param>
        public Incrementor(int size = 10)
        {
            if(size < 0)
                throw new ArgumentOutOfRangeException(nameof(size), "Must be non-negative.");

            Data = new bool[size];
            Overflow = size == 0;

            if (Size > 0)
                Data[0] = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Move to other option
        /// </summary>
        public void Increment()
        {
            var overflow = true;

            for (var i = 0; i < Size && overflow; i++)
            {
                overflow = this[i];
                this[i] = !this[i];
            }

            Overflow = overflow;
        }

        #endregion
    }
}