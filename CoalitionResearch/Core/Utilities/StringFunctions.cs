﻿using System.Collections.Generic;
using System.Text;

namespace CoalitionResearch.Core.Utilities
{
    /// <summary>
    /// Functions that generates strings
    /// </summary>
    public static class StringFunctions
    {
        /// <summary>
        /// Generate a list of items as a string.
        /// </summary>
        /// <typeparam name="T">Any Type</typeparam>
        /// <param name="lst">List of items</param>
        /// <param name="delimiter">The delimiter of the items (default ',')</param>
        /// <returns>String with the list of items delimited</returns>
        public static string CreateListString<T>(IEnumerable<T> lst, char delimiter = ',')
        {
            if (lst == null) return "";

            var builder = new StringBuilder();
            var isNotBeginning = false;

            foreach (var obj in lst)
            {
                if (isNotBeginning)
                    builder.Append(delimiter);
                else
                    isNotBeginning = true;

                builder.Append(obj);
            }
            
            return builder.ToString();
        }
    }
}