﻿using System.Collections.Generic;

namespace CoalitionResearch.Records
{
    /// <summary>
    /// Aggregation record for an basic simulation
    /// </summary>
    public class BasicSimulationRecord : SimulationRecord
    {
        #region Properties
        /// <summary>
        /// Noise Numerator for the prototypes
        /// For Example: if np = 0.25 = 1/4, then Noise = 1
        /// </summary>
        public int NoisePrototypes { get; set; }
        /// <summary>
        /// Noise Numerator for the voters
        /// For Example: if nv = 0.4 = 2/5, then Base = 2
        /// </summary>
        public int NoiseVoters { get; set; }
        /// <summary>
        /// Noise Denominator of the Prototypes
        /// For Example: if np = 0.25 = 1/4, then Base = 4
        /// </summary>
        public int NoisePrototypesBase { get; set; } = 10;

        /// <summary>
        /// Noise Denominator of the Voters
        /// For Example: if nv = 0.2 = 1/5, then Base = 5
        /// </summary>
        public int NoiseVotersBase { get; set; } = 10;
        #endregion

        #region Methods
        protected override void WriteHeaderMetadata(ICollection<object> lst)
        {
            lst.Add(nameof(NoisePrototypes));
            lst.Add(nameof(NoiseVoters));
        }

        protected override void WriteHeaderStats(ICollection<object> lst)
        {
        }

        protected override void WriteMetadata(ICollection<object> lst)
        {
            lst.Add((double)NoisePrototypes / NoisePrototypesBase);
            lst.Add((double)NoiseVoters / NoiseVotersBase);
        }

        protected override void WriteStats(ICollection<object> lst)
        {
        }
        #endregion
    }
}