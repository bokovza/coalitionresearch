﻿using CoalitionResearch.Core;
using CoalitionResearch.Simulations.Division;
using CoalitionResearch.Utilities;
using static CoalitionResearch.Records.RecordService;

namespace CoalitionResearch.Records
{
    /// <summary>
    /// Record for a single run
    /// </summary>
    public class DividerRunRecord : IRecord
    {
        #region Simple Properties
        /// <summary>
        /// Run Identification Numbers
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Amount of Iterations that Stablize (Algorithm 1) took
        /// </summary>
        public int Iterations { get; set; }
        /// <summary>
        /// Runtime of the run [miliseconds]
        /// </summary>
        public long Runtime { get; set; }
        /// <summary>
        /// Amount of voters - no serialized (only for calculation)
        /// </summary>
        public int Voters { get; set; }
        #endregion

        #region Measurements
        /// <summary>
        /// Rand Measure
        /// Before - Voters divided by chosen prototypes
        /// After - Voters divided by parties after running "Stablize"
        /// </summary>
        public double RandMeasurement { get; set; }
        /// <summary>
        /// Amount of parties after running "Stablize" Method
        /// </summary>
        public int PartiesAfter { get; set; }

        /// <summary>
        /// Amount of voters in the coalition
        /// </summary>
        public int Distributions { get; set; }
        

        #region Implementation
        /// <summary>
        /// Writing the record's header into the CSV file
        /// </summary>
        /// <param name="writer">An open CSV file for writing</param>
        public void WriteHeader(CsvWriter writer)
        {
            var tmp = new string[]
            {
               nameof(Id),
               nameof(Iterations),
               nameof(Runtime),

               nameof(RandMeasurement),
               nameof(PartiesAfter),
               nameof(Distributions)
            };

            var lst = PrintNames(tmp);

            writer?.Write(lst);
        }
        /// <summary>
        /// Writing the record's data into the CSV file
        /// </summary>
        /// <param name="writer">An open CSV file for writing</param>
        public void Write(CsvWriter writer)
        {
            writer?.Write(new object[]
            {
               Id,
               Iterations,
               Runtime,

               RandMeasurement,
               PartiesAfter,
               Distributions
               });
        }
        #endregion

        #region Insert Results To Record
        /// <summary>
        /// Executing Stablize Method on the context, and load the results into the record
        /// </summary>
        /// <param name="context">Voting Context</param>
        public void Stablize(VotingContext context)
        {
            if (context == null)
                return;

            Iterations = context.Stablize();
            PartiesAfter = context.Parties.Count;

            var checker = new Checker(context);
            checker.Check();
            Distributions = checker.Count;
        }
        #endregion
        #endregion
    }
}
