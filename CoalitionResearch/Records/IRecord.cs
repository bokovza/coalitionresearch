﻿using CoalitionResearch.Utilities;

namespace CoalitionResearch.Records
{
    /// <summary>
    /// Interface for CSV record classes
    /// </summary>
    public interface IRecord
    {
        /// <summary>
        /// Write the header of the csv file.
        /// </summary>
        /// <param name="writer">The CSV file to write to.</param>
        void WriteHeader(CsvWriter writer);

        /// <summary>
        /// Write the record's data into the csv file.
        /// </summary>
        /// <param name="writer">The CSV file to write to.</param>
        void Write(CsvWriter writer);
    }
}