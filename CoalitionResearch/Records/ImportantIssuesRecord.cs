﻿using System.Collections.Generic;

namespace CoalitionResearch.Records
{
    /// <summary>
    /// Aggregation record for an important issues simulation
    /// </summary>
    public class ImportantIssuesRecord : BasicSimulationRecord
    {
        #region Properties
        /// <summary>
        /// Amount of important issues
        /// </summary>
        public int ImportantIssues { get; set; }
        #endregion

        #region Methods
        protected override void WriteHeaderMetadata(ICollection<object> lst)
        {
            base.WriteHeaderMetadata(lst);
            lst.Add(nameof(ImportantIssues));
        }

        protected override void WriteMetadata(ICollection<object> lst)
        {
            base.WriteMetadata(lst);
            lst.Add(ImportantIssues);
        }
        #endregion
    }
}