﻿using System.Collections.Generic;
using CoalitionResearch.Utilities;
using static System.Math;
using static CoalitionResearch.Records.RecordService;
using CoalitionResearch.Utilities.Aggregators;
using System.Linq;

namespace CoalitionResearch.Records
{
    /// <summary>
    /// Aggregation simulation record
    /// </summary>
    public abstract class SimulationRecord : IRecord
    {
        #region Members
        /// <summary>
        /// List of Accumulators
        /// </summary>
        private readonly ICollection<IAggregator> _accumulators = new List<IAggregator>();
        #endregion

        #region Properties
        /// <summary>
        /// Parameter Alpha of the Beta Distribution
        /// </summary>
        public double Alpha { get; set; } = 1;
        /// <summary>
        /// Parameter Beta of the Beta Distribution
        /// </summary>
        public double Beta { get; set; } = 1;
        /// <summary>
        /// Amount of issues
        /// </summary>
        public int Issues { get; set; }
        /// <summary>
        /// Amount of runs
        /// </summary>
        public int Runs { get; set; }
        /// <summary>
        /// The electoral threshold
        /// </summary>
        public double ElectoralThreshold { get; set; }
        /// <summary>
        /// Amount of parties
        /// </summary>
        public int Parties { get; set; }
        /// <summary>
        /// Amount of voters
        /// </summary>
        public int Voters { get; set; }

        /// <summary>
        /// Total Runtime [miliseconds]
        /// </summary>
        public long Runtime { get; set; }

        /// <summary>
        /// The variance of the beta distribution
        /// </summary>
        public double BetaDistributionVariance => Alpha * Beta / (Pow(Alpha + Beta, 2) * (Alpha + Beta + 1));
        #endregion

        #region Accumulators
        #region Basic
        /// <summary>
        /// "Iterations" Property's Aggregator 
        /// </summary>
        public DoubleAggregator Iterations { get; } = new DoubleAggregator(nameof(Iterations));
        /// <summary>
        /// "RandMeasure" Property's Aggregator 
        /// </summary>
        public DoubleAggregator RandMeasurement { get; } = new DoubleAggregator(nameof(RandMeasurement));
        /// <summary>
        /// "PartiesAfter" Property's Aggregator 
        /// </summary>
        public DoubleAggregator PartiesAfter { get; } = new DoubleAggregator(nameof(PartiesAfter));

        /// <summary>
        /// "LeaderIndex" Property's Aggregator 
        /// </summary>
        public IndexAggregator LeaderIndex { get; } = new IndexAggregator(nameof(LeaderIndex));
        /// <summary>
        /// "CoalitionVoters" Property's Aggregator 
        /// </summary>
        public DoubleAggregator CoalitionVoters { get; } = new DoubleAggregator(nameof(CoalitionVoters));
        /// <summary>
        /// "CoalitionParties" Property's Aggregator 
        /// </summary>
        public DoubleAggregator CoalitionParties { get; } = new DoubleAggregator(nameof(CoalitionParties));
        /// <summary>
        /// "ProperCoalitions" Property's Aggregator 
        /// </summary>
        public DoubleAggregator ProperCoalitions { get; } = new DoubleAggregator(nameof(ProperCoalitions));
        /// <summary>
        /// "CoreCoalitions" Property's Aggregator 
        /// </summary>
        public DoubleAggregator CoreCoalitions { get; } = new DoubleAggregator(nameof(CoreCoalitions));
        /// <summary>
        /// "DroppedCoalitions" Property's Aggregator 
        /// </summary>
        public DoubleAggregator DroppedCoalitions { get; } = new DoubleAggregator(nameof(DroppedCoalitions));
        /// <summary>
        /// "IsMininalCardinals" Property's Aggregator 
        /// </summary>
        public ConditionAggregator IsMininalCardinals { get; } = new ConditionAggregator(nameof(IsMininalCardinals));
        /// <summary>
        /// ConditionAggregator - Amount of times CoreCoalitions > 0
        /// </summary>
        public ConditionAggregator CoreExists { get; } = new ConditionAggregator(nameof(CoreExists));
        #endregion

        #region Values
        /// <summary>
        /// "CoalitionValue" Property's Aggregator 
        /// </summary>
        public DoubleAggregator CoalitionValue { get; } = new DoubleAggregator(nameof(CoalitionValue));
        /// <summary>
        /// "OppositionValue" Property's Aggregator 
        /// </summary>
        public DoubleAggregator OppositionValue { get; } = new DoubleAggregator(nameof(OppositionValue));
        /// <summary>
        /// "TotalValue" Property's Aggregator 
        /// </summary>
        public DoubleAggregator TotalValue { get; } = new DoubleAggregator(nameof(TotalValue));
        /// <summary>
        /// "SinglePartyValue" Property's Aggregator 
        /// </summary>
        public DoubleAggregator SinglePartyValue { get; } = new DoubleAggregator(nameof(SinglePartyValue));

        /// <summary>
        /// "CoalitionSinglePartyValue" Property's Aggregator 
        /// </summary>
        public DoubleAggregator CoalitionSinglePartyValue { get; } = new DoubleAggregator(nameof(CoalitionSinglePartyValue));
        /// <summary>
        /// "OppositionSinglePartyValue" Property's Aggregator 
        /// </summary>
        public DoubleAggregator OppositionSinglePartyValue { get; } = new DoubleAggregator(nameof(OppositionSinglePartyValue));
        #endregion

        #region Values - Ratios
        /// <summary>
        /// "CoalitionValueRatio" Property's Aggregator 
        /// </summary>
        public DoubleAggregator CoalitionValueRatio { get; } = new DoubleAggregator(nameof(CoalitionValueRatio));
        /// <summary>
        /// "OppositionValueRatio" Property's Aggregator 
        /// </summary>
        public DoubleAggregator OppositionValueRatio { get; } = new DoubleAggregator(nameof(OppositionValueRatio));
        /// <summary>
        /// "SinglePartyValueRatio" Property's Aggregator 
        /// </summary>
        public DoubleAggregator SinglePartyValueRatio { get; } = new DoubleAggregator(nameof(SinglePartyValueRatio));
        #endregion

        #region Values - Overflow
        /// <summary>
        /// "CoalitionValueOverflow" Property's Aggregator 
        /// </summary>
        public ConditionAggregator CoalitionValueOverflow { get; } = new ConditionAggregator(nameof(CoalitionValueOverflow));
        /// <summary>
        /// "OppositionValueOverflow" Property's Aggregator 
        /// </summary>
        public ConditionAggregator OppositionValueOverflow { get; } = new ConditionAggregator(nameof(OppositionValueOverflow));
        /// <summary>
        /// "SinglePartyValueOverflow" Property's Aggregator 
        /// </summary>
        public ConditionAggregator SinglePartyValueOverflow { get; } = new ConditionAggregator(nameof(SinglePartyValueOverflow));
        #endregion

        #region Values 2 - Ratios
        /// <summary>
        /// "CoalitionSinglePartyRatio" Property's Aggregator 
        /// </summary>
        public DoubleAggregator CoalitionSinglePartyRatio { get; } = new DoubleAggregator(nameof(CoalitionSinglePartyRatio));
        /// <summary>
        /// "OppositionSinglePartyRatio" Property's Aggregator 
        /// </summary>
        public DoubleAggregator OppositionSinglePartyRatio { get; } = new DoubleAggregator(nameof(OppositionSinglePartyRatio));
        /// <summary>
        /// "TotalSinglePartyRatio" Property's Aggregator 
        /// </summary>
        public DoubleAggregator TotalSinglePartyRatio { get; } = new DoubleAggregator(nameof(TotalSinglePartyRatio));
        #endregion

        #region Value 2 - Overflows
        /// <summary>
        /// "CoalitionSinglePartyOverflow" Property's Aggregator 
        /// </summary>
        public ConditionAggregator CoalitionSinglePartyOverflow { get; } = new ConditionAggregator(nameof(CoalitionSinglePartyOverflow));
        /// <summary>
        /// "OppositionSinglePartyOverflow" Property's Aggregator 
        /// </summary>
        public ConditionAggregator OppositionSinglePartyOverflow { get; } = new ConditionAggregator(nameof(OppositionSinglePartyOverflow));
        /// <summary>
        /// "TotalSinglePartyOverflow" Property's Aggregator 
        /// </summary>
        public ConditionAggregator TotalSinglePartyOverflow { get; } = new ConditionAggregator(nameof(TotalSinglePartyOverflow));
        #endregion

        #region Voter Values

        /// <summary>
        /// "CoalitionVoterValue" Property's Aggregator 
        /// </summary>
        public DoubleAggregator CoalitionVoterValue { get; } = new DoubleAggregator(nameof(CoalitionVoterValue));
        /// <summary>
        /// "OppositionVoterValue" Property's Aggregator 
        /// </summary>
        public DoubleAggregator OppositionVoterValue { get; } = new DoubleAggregator(nameof(OppositionVoterValue));
        /// <summary>
        /// "TotalVoterValue" Property's Aggregator 
        /// </summary>
        public DoubleAggregator TotalVoterValue { get; } = new DoubleAggregator(nameof(TotalVoterValue));
        /// <summary>
        /// "SinglePartyVoterValue" Property's Aggregator 
        /// </summary>
        public DoubleAggregator SinglePartyVoterValue { get; } = new DoubleAggregator(nameof(SinglePartyVoterValue));

        /// <summary>
        /// "CoalitionSinglePartyVoterValue" Property's Aggregator 
        /// </summary>
        public DoubleAggregator CoalitionSinglePartyVoterValue { get; } = new DoubleAggregator(nameof(CoalitionSinglePartyVoterValue));
        /// <summary>
        /// "OppositionSinglePartyVoterValue" Property's Aggregator 
        /// </summary>
        public DoubleAggregator OppositionSinglePartyVoterValue { get; } = new DoubleAggregator(nameof(OppositionSinglePartyVoterValue));
        #endregion

        #region Conditions
        /// <summary>
        /// "SingleTotalCondition" Property's Aggregator 
        /// </summary>
        public ConditionAggregator SingleTotalCondition { get; } = new ConditionAggregator(nameof(SingleTotalCondition));
        /// <summary>
        /// "CoalitionSingleCoalitionCondition" Property's Aggregator 
        /// </summary>
        public ConditionAggregator CoalitionSingleCoalitionCondition { get; } = new ConditionAggregator(nameof(CoalitionSingleCoalitionCondition));
        /// <summary>
        /// "SingleOppositionOppositionCondition" Property's Aggregator 
        /// </summary>
        public ConditionAggregator SingleOppositionOppositionCondition { get; } = new ConditionAggregator(nameof(SingleOppositionOppositionCondition));
        #endregion

        #region Voter Values - Ratios
        /// <summary>
        /// "CoalitionVoterValueRatio" Property's Aggregator 
        /// </summary>
        public DoubleAggregator CoalitionVoterValueRatio { get; } = new DoubleAggregator(nameof(CoalitionVoterValueRatio));
        /// <summary>
        /// "OppositionVoterValueRatio" Property's Aggregator 
        /// </summary>
        public DoubleAggregator OppositionVoterValueRatio { get; } = new DoubleAggregator(nameof(OppositionVoterValueRatio));
        /// <summary>
        /// "SinglePartyVoterValueRatio" Property's Aggregator 
        /// </summary>
        public DoubleAggregator SinglePartyVoterValueRatio { get; } = new DoubleAggregator(nameof(SinglePartyVoterValueRatio));

        /// <summary>
        /// "CoalitionSinglePartyVoterValueRatio" Property's Aggregator 
        /// </summary>
        public DoubleAggregator CoalitionSinglePartyVoterValueRatio { get; } = new DoubleAggregator(nameof(CoalitionSinglePartyVoterValueRatio));
        /// <summary>
        /// "OppositionSinglePartyVoterValueRatio" Property's Aggregator 
        /// </summary>
        public DoubleAggregator OppositionSinglePartyVoterValueRatio { get; } = new DoubleAggregator(nameof(OppositionSinglePartyVoterValueRatio));
        #endregion

        #endregion

        #region Conditions
        #region Average Conditions
        /// <summary>
        /// Average Condition 1
        /// Utility : Single Party >= Total
        /// Regret : Total >= Single Party
        /// </summary>
        public bool AverageSingleTotalCondition => ConditionValue(TotalVoterValue.Result, SinglePartyVoterValue.Result);
        /// <summary>
        /// Average Condition 2
        /// Utility : Coalition >= Coalition Single Party
        /// Regret : Coalition Single Party >= Coalition
        /// </summary>
        public bool AverageCoalitionSingleCoalitionCondition => ConditionValue(CoalitionSinglePartyVoterValue.Result, CoalitionVoterValue.Result);
        /// <summary>
        /// Average Condition 3
        /// Utility : Opposition Single Party >= Opposition
        /// Regret : Opposition >= Opposition Single Party
        /// </summary>
        public bool AverageSingleOppositionOppositionCondition => ConditionValue(OppositionVoterValue.Result, OppositionSinglePartyVoterValue.Result);
        #endregion

        #region All Conditions
        /// <summary>
        /// Is Condition 1 was true in all cases ?
        /// </summary>
        public bool AllSingleTotalCondition => CheckIs1(SingleTotalCondition.Result);
        /// <summary>
        ///  Is Condition 2 was true in all cases ?
        /// </summary>
        public bool AllCoalitionSingleCoalitionCondition => CheckIs1(CoalitionSingleCoalitionCondition.Result);
        /// <summary>
        /// Is Condition 3 was true in all cases ?
        /// </summary>
        public bool AllSingleOppositionOppositionCondition => CheckIs1(SingleOppositionOppositionCondition.Result);
        #endregion
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        public SimulationRecord()
        {
            foreach (var property in typeof(SimulationRecord).GetProperties().Select(prop => prop.GetValue(this) as IAggregator).Where(item => item != null))
                _accumulators.Add(property);
        }
        #endregion

        #region Abstract Implementation
        /// <summary>
        /// Writes the header of the record into the CSV File
        /// </summary>
        /// <param name="writer">An open CSV File for writing</param>
        public void WriteHeader(CsvWriter writer)
        {
            var lst = new List<object>
            {
                nameof(Alpha),
                nameof(Beta),
                nameof(Issues),
                nameof(Voters),
                nameof(Runs),
                nameof(Parties),
                nameof(BetaDistributionVariance),
                nameof(Runtime)
            };

            WriteHeaderMetadata(lst);

            foreach (var accumulator in _accumulators)
                lst.Add(accumulator.Name);

            lst.Add(ConditionName(nameof(TotalVoterValue), nameof(SinglePartyVoterValue)));
            lst.Add(ConditionName(nameof(CoalitionSinglePartyVoterValue), nameof(CoalitionVoterValue)));
            lst.Add(ConditionName(nameof(OppositionVoterValue), nameof(OppositionSinglePartyVoterValue)));

            lst.Add(PrintName(nameof(AllSingleTotalCondition)));
            lst.Add(PrintName(nameof(AllCoalitionSingleCoalitionCondition)));
            lst.Add(PrintName(nameof(AllSingleOppositionOppositionCondition)));

            WriteHeaderStats(lst);

            writer?.Write(lst);
        }

        /// <summary>
        /// Writes the extra metadata's header to the file
        /// </summary>
        /// <param name="lst">List of names for the file</param>
        protected abstract void WriteHeaderMetadata(ICollection<object> lst);
        /// <summary>
        /// Writes the extra statistics's header to the file
        /// </summary>
        /// <param name="lst">List of names for the file</param>
        protected abstract void WriteHeaderStats(ICollection<object> lst);

        /// <summary>
        /// Writes the data of the record into the CSV File
        /// </summary>
        /// <param name="writer">An open CSV File for writing</param>
        public void Write(CsvWriter writer)
        {
            var lst = new List<object>
            {
                Alpha,
                Beta,
                Issues,
                Voters,
                Runs,
                Parties,
                BetaDistributionVariance,
                Runtime
            };

            WriteMetadata(lst);

            foreach (var accumulator in _accumulators)
                lst.Add(accumulator.Result);

            lst.Add(PrintBool(AverageSingleTotalCondition));
            lst.Add(PrintBool(AverageCoalitionSingleCoalitionCondition));
            lst.Add(PrintBool(AverageSingleOppositionOppositionCondition));

            lst.Add(PrintBool(AllSingleTotalCondition));
            lst.Add(PrintBool(AllCoalitionSingleCoalitionCondition));
            lst.Add(PrintBool(AllSingleOppositionOppositionCondition));

            WriteStats(lst);

            writer?.Write(lst);
        }

        /// <summary>
        /// Writes the extra metadata's data to the file
        /// </summary>
        /// <param name="lst">List of values for the file</param>
        protected abstract void WriteMetadata(ICollection<object> lst);
        /// <summary>
        /// Writes the extra statistics' data to the file
        /// </summary>
        /// <param name="lst">List of values for the file</param>
        protected abstract void WriteStats(ICollection<object> lst);
        #endregion

        #region Methods
        /// <summary>
        /// Reseting the record
        /// </summary>
        public virtual void Reset()
        {
            Runtime = 0;

            foreach (var accumulator in _accumulators)
                accumulator.Reset();
        }

        /// <summary>
        /// Gets a RunRecord and accumulating all values needed
        /// </summary>
        /// <param name="record">The record to accumulate</param>
        public virtual void Accumulate(RunRecord record)
        {
            if (record == null)
                return;

            Iterations.Add(record.Iterations);

            RandMeasurement.Add(record.RandMeasurement);
            PartiesAfter.Add(record.PartiesAfter);

            LeaderIndex.Add(record.LeaderIndex);
            CoalitionVoters.Add(record.CoalitionVoters);
            CoalitionParties.Add(record.CoalitionParties);

            ProperCoalitions.Add(record.ProperCoalitions);
            CoreCoalitions.Add(record.CoreCoalitions);
            DroppedCoalitions.Add(record.DroppedCoalitions);
            IsMininalCardinals.Add(record.IsMininalCardinals);

            CoreExists.Add(record.CoreCoalitions > 0);

            CoalitionValue.Add(record.CoalitionValue);
            OppositionValue.Add(record.OppositionValue);
            TotalValue.Add(record.TotalValue);
            SinglePartyValue.Add(record.SinglePartyValue);

            CoalitionSinglePartyValue.Add(record.CoalitionSinglePartyValue);
            OppositionSinglePartyValue.Add(record.OppositionSinglePartyValue);

            CoalitionValueRatio.Add(record.CoalitionValueRatio);
            OppositionValueRatio.Add(record.OppositionValueRatio);
            SinglePartyValueRatio.Add(record.SinglePartyValueRatio);

            CoalitionValueOverflow.Add(record.CoalitionValueOverflow);
            OppositionValueOverflow.Add(record.OppositionValueOverflow);
            SinglePartyValueOverflow.Add(record.SinglePartyValueOverflow);

            CoalitionSinglePartyRatio.Add(record.CoalitionSinglePartyRatio);
            OppositionSinglePartyRatio.Add(record.OppositionSinglePartyRatio);
            TotalSinglePartyRatio.Add(record.TotalSinglePartyRatio);

            CoalitionSinglePartyOverflow.Add(record.CoalitionSinglePartyOverflow);
            OppositionSinglePartyOverflow.Add(record.OppositionSinglePartyOverflow);
            TotalSinglePartyOverflow.Add(record.TotalSinglePartyOverflow);

            CoalitionVoterValue.Add(record.CoalitionVoterValue);
            OppositionVoterValue.Add(record.OppositionVoterValue);
            TotalVoterValue.Add(record.TotalVoterValue);
            SinglePartyVoterValue.Add(record.SinglePartyVoterValue);

            CoalitionSinglePartyVoterValue.Add(record.CoalitionSinglePartyVoterValue);
            OppositionSinglePartyVoterValue.Add(record.OppositionSinglePartyVoterValue);

            SingleTotalCondition.Add(record.SingleTotalCondition);
            CoalitionSingleCoalitionCondition.Add(record.CoalitionSingleCoalitionCondition);
            SingleOppositionOppositionCondition.Add(record.SingleOppositionOppositionCondition);

            CoalitionVoterValueRatio.Add(record.CoalitionVoterValueRatio);
            OppositionVoterValueRatio.Add(record.OppositionVoterValueRatio);
            SinglePartyVoterValueRatio.Add(record.SinglePartyVoterValueRatio);

            CoalitionSinglePartyVoterValueRatio.Add(record.CoalitionSinglePartyVoterValueRatio);
            OppositionSinglePartyVoterValueRatio.Add(record.OppositionSinglePartyVoterValueRatio);
        }
        #endregion
    }
}