﻿using CoalitionResearch.Core;
using CoalitionResearch.Utilities;
using static CoalitionResearch.Records.RecordService;

namespace CoalitionResearch.Records
{
    /// <summary>
    /// Record for a single run
    /// </summary>
    public class RunRecord : IRecord
    {
        #region Simple Properties
        /// <summary>
        /// Run Identification Numbers
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Amount of Iterations that Stablize (Algorithm 1) took
        /// </summary>
        public int Iterations { get; set; }
        /// <summary>
        /// Runtime of the run [miliseconds]
        /// </summary>
        public long Runtime { get; set; }
        /// <summary>
        /// Amount of voters - no serialized (only for calculation)
        /// </summary>
        public int Voters { get; set; }
        #endregion

        #region Measurements
        /// <summary>
        /// Rand Measure
        /// Before - Voters divided by chosen prototypes
        /// After - Voters divided by parties after running "Stablize"
        /// </summary>
        public double RandMeasurement { get; set; }
        /// <summary>
        /// Amount of parties after running "Stablize" Method
        /// </summary>
        public int PartiesAfter { get; set; }
        /// <summary>
        /// Index of the coalition's leader
        /// 0 - Biggest Party
        /// 1 - 2nd biggest Party
        /// (-1) - No coalition
        /// </summary>
        public int LeaderIndex { get; set; }
        /// <summary>
        /// Amount of voters in the coalition
        /// </summary>
        public int CoalitionVoters { get; set; }
        /// <summary>
        /// Amount of parties in the coalition
        /// </summary>
        public int CoalitionParties { get; set; }
        /// <summary>
        /// Amount of possible proper coalitions
        /// </summary>
        public int ProperCoalitions { get; set; }
        /// <summary>
        /// Amount of Core Coalitions
        /// </summary>
        public double CoreCoalitions { get; set; }
        /// <summary>
        /// Amount of Proper Coalitions that are not in the core
        /// </summary>
        public double DroppedCoalitions => ProperCoalitions - CoreCoalitions;
        /// <summary>
        /// Is this chosen coalition got the minimum amount of parties ?
        /// </summary>
        public bool IsMininalCardinals { get; set; }
        #endregion

        #region Values
        /// <summary>
        /// Sum of Values(Utilities/Regrets) for the parties that are in the coalition
        /// </summary>
        public double CoalitionValue { get; set; }
        /// <summary>
        /// Sum of Values(Utilities/Regrets) for the parties that aren't in the coalition
        /// </summary>
        public double OppositionValue { get; set; }
        /// <summary>
        /// Sum of Values(Utilities/Regrets) for all parties
        /// </summary>
        public double TotalValue => CoalitionValue + OppositionValue;
        /// <summary>
        /// The Single Party Value
        /// </summary>
        public double SinglePartyValue { get; set; }

        /// <summary>
        /// Sum of Coalition's Voters, if they were in the Single Party
        /// </summary>
        public double CoalitionSinglePartyValue { get; set; }
        /// <summary>
        /// Sum of Opposition's Voters, if they were in the Single Party
        /// </summary>
        public double OppositionSinglePartyValue { get; set; }
        #endregion

        #region Ratios
        /// <summary>
        /// CoalitionValue / Total Value
        /// </summary>
        public double CoalitionValueRatio => CoalitionRatio(CoalitionValue, TotalValue);
        /// <summary>
        /// OppositionValue / Total Value
        /// </summary>
        public double OppositionValueRatio => CoalitionRatio(OppositionValue, TotalValue);
        /// <summary>
        /// SinglePartyValue / Total Value
        /// </summary>
        public double SinglePartyValueRatio => CoalitionRatio(SinglePartyValue, TotalValue);

        /// <summary>
        /// CoalitionSinglePartyValue / SinglePartyValue
        /// </summary>
        public double CoalitionSinglePartyRatio => CoalitionRatio(CoalitionValue, SinglePartyValue);
        /// <summary>
        /// OppositionSinglePartyValue / SinglePartyValue
        /// </summary>
        public double OppositionSinglePartyRatio => CoalitionRatio(OppositionValue, SinglePartyValue);
        /// <summary>
        /// TotalValue / SinglePartyValue
        /// </summary>
        public double TotalSinglePartyRatio => CoalitionRatio(TotalValue, SinglePartyValue);
        #endregion

        #region Overflows
        /// <summary>
        /// True if CoalitionValueRatio > 1
        /// </summary>
        public bool CoalitionValueOverflow => CheckOverflow(CoalitionValueRatio);
        /// <summary>
        /// True if OppositionValueRatio > 1
        /// </summary>
        public bool OppositionValueOverflow => CheckOverflow(OppositionValueRatio);
        /// <summary>
        /// True if SinglePartyValueRatio > 1
        /// </summary>
        public bool SinglePartyValueOverflow => CheckOverflow(SinglePartyValueRatio);
        
        /// <summary>
        /// True if CoalitionSinglePartyValueRatio > 1
        /// </summary>
        public bool CoalitionSinglePartyOverflow => CheckOverflow(CoalitionSinglePartyRatio);
        /// <summary>
        /// True if OppositionSinglePartyValueRatio > 1
        /// </summary>
        public bool OppositionSinglePartyOverflow => CheckOverflow(OppositionSinglePartyRatio);
        /// <summary>
        /// True if TotalSinglePartyValueRatio > 1
        /// </summary>
        public bool TotalSinglePartyOverflow => CheckOverflow(TotalSinglePartyRatio);
        #endregion

        #region Voter Value
        /// <summary>
        /// Coalition Value per voter (in the coalition)
        /// </summary>
        public double CoalitionVoterValue => CoalitionRatio(CoalitionValue, CoalitionVoters);
        /// <summary>
        /// Opposition Value per voter (in the opposition)
        /// </summary>
        public double OppositionVoterValue => CoalitionRatio(OppositionValue, Voters - CoalitionVoters);
        /// <summary>
        /// Single Party Value per voter
        /// </summary>
        public double SinglePartyVoterValue => CoalitionRatio(SinglePartyValue, Voters);
        /// <summary>
        /// Total Value per voter
        /// </summary>
        public double TotalVoterValue => CoalitionRatio(TotalValue, Voters);

        /// <summary>
        /// CoalitionSinglePartyValue per voter (in the coalition)
        /// </summary>
        public double CoalitionSinglePartyVoterValue => CoalitionRatio(CoalitionSinglePartyValue, CoalitionVoters);
        /// <summary>
        /// OppositionSinglePartyValue per voter (in the opposition)
        /// </summary>
        public double OppositionSinglePartyVoterValue => CoalitionRatio(OppositionSinglePartyValue, Voters - CoalitionVoters);

        #endregion

        #region Voter Value - Ratios
        /// <summary>
        /// CoalitionVoterValue / TotalVoterValue
        /// </summary>
        public double CoalitionVoterValueRatio => CoalitionRatio(CoalitionVoterValue, TotalVoterValue);
        /// <summary>
        /// OppositionVoterValue / TotalVoterValue
        /// </summary>
        public double OppositionVoterValueRatio => CoalitionRatio(OppositionVoterValue, TotalVoterValue);
        /// <summary>
        /// SinglePartyVoterValue / TotalVoterValue
        /// </summary>
        public double SinglePartyVoterValueRatio => CoalitionRatio(SinglePartyVoterValue, TotalVoterValue);

        /// <summary>
        /// CoalitionSinglePartyVoterValue / TotalVoterValue
        /// </summary>
        public double CoalitionSinglePartyVoterValueRatio => CoalitionRatio(CoalitionSinglePartyVoterValue, TotalVoterValue);
        /// <summary>
        /// OppositionSinglePartyVoterValue / TotalVoterValue
        /// </summary>
        public double OppositionSinglePartyVoterValueRatio => CoalitionRatio(OppositionSinglePartyVoterValue, TotalVoterValue);
        #endregion

        #region Conditions
        /// <summary>
        /// Condition 1
        /// Utility : Single Party >= Total
        /// Regret : Total >= Single Party
        /// </summary>
        public bool SingleTotalCondition => ConditionValue(TotalVoterValue, SinglePartyVoterValue);
        /// <summary>
        /// Condition 2
        /// Utility : Coalition >= Coalition Single Party
        /// Regret : Coalition Single Party >= Coalition
        /// </summary>
        public bool CoalitionSingleCoalitionCondition => ConditionValue(CoalitionSinglePartyVoterValue, CoalitionVoterValue);

        /// <summary>
        /// Condition 3
        /// Utility : Opposition Single Party >= Opposition
        /// Regret : Opposition >= Opposition Single Party
        /// </summary>
        public bool SingleOppositionOppositionCondition => ConditionValue(OppositionVoterValue, OppositionSinglePartyVoterValue);
        #endregion

        #region Implementation
        /// <summary>
        /// Writing the record's header into the CSV file
        /// </summary>
        /// <param name="writer">An open CSV file for writing</param>
        public void WriteHeader(CsvWriter writer)
        {
            var tmp = new string[]
            {
               nameof(Id),
               nameof(Iterations),
               nameof(Runtime),

               nameof(RandMeasurement),
               nameof(PartiesAfter),
               nameof(LeaderIndex),
               nameof(CoalitionVoters),
               nameof(CoalitionParties),
               nameof(ProperCoalitions),
               nameof(CoreCoalitions),
               nameof(DroppedCoalitions),
               nameof(IsMininalCardinals),

               nameof(CoalitionValue),
               nameof(OppositionValue),
               nameof(TotalValue),
               nameof(SinglePartyValue),
               nameof(CoalitionSinglePartyValue),
               nameof(OppositionSinglePartyValue),

               nameof(CoalitionValueRatio),
               nameof(OppositionValueRatio),
               nameof(SinglePartyValueRatio),

               nameof(CoalitionValueOverflow),
               nameof(OppositionValueOverflow),
               nameof(SinglePartyValueOverflow),

               nameof(CoalitionSinglePartyRatio),
               nameof(OppositionSinglePartyRatio),
               nameof(TotalSinglePartyRatio),

               nameof(CoalitionSinglePartyOverflow),
               nameof(OppositionSinglePartyOverflow),
               nameof(TotalSinglePartyOverflow),

               nameof(CoalitionVoterValue),
               nameof(OppositionVoterValue),
               nameof(TotalVoterValue),
               nameof(SinglePartyVoterValue),

               nameof(CoalitionSinglePartyValue),
               nameof(OppositionSinglePartyValue),

               nameof(CoalitionVoterValueRatio),
               nameof(OppositionVoterValueRatio),
               nameof(SinglePartyValueRatio),

               nameof(CoalitionSinglePartyVoterValueRatio),
               nameof(OppositionSinglePartyVoterValueRatio),
            };

            var lst = PrintNames(tmp);

            lst?.Add(ConditionName(nameof(TotalVoterValue), nameof(SinglePartyVoterValue)));
            lst?.Add(ConditionName(nameof(CoalitionSinglePartyVoterValue), nameof(CoalitionVoterValue)));
            lst?.Add(ConditionName(nameof(OppositionVoterValue), nameof(OppositionSinglePartyVoterValue)));

            writer?.Write(lst);
        }
        /// <summary>
        /// Writing the record's data into the CSV file
        /// </summary>
        /// <param name="writer">An open CSV file for writing</param>
        public void Write(CsvWriter writer)
        {
            writer?.Write(new object[]
            {
               Id,
               Iterations,
               Runtime,

               RandMeasurement,
               PartiesAfter,
               LeaderIndex,
               CoalitionVoters,
               CoalitionParties,
               ProperCoalitions,
               CoreCoalitions,
               DroppedCoalitions,
               PrintBool(IsMininalCardinals),

               CoalitionValue,
               OppositionValue,
               TotalValue,
               SinglePartyValue,
               CoalitionSinglePartyValue,
               OppositionSinglePartyValue,

               CoalitionValueRatio,
               OppositionValueRatio,
               SinglePartyValueRatio,

               PrintBool(CoalitionValueOverflow),
               PrintBool(OppositionValueOverflow),
               PrintBool(SinglePartyValueOverflow),

               CoalitionSinglePartyRatio,
               OppositionSinglePartyRatio,
               TotalSinglePartyRatio,

               PrintBool(CoalitionSinglePartyOverflow),
               PrintBool(OppositionSinglePartyOverflow),
               PrintBool(TotalSinglePartyOverflow),

               CoalitionVoterValue,
               OppositionVoterValue,
               TotalVoterValue,
               SinglePartyVoterValue,

               CoalitionSinglePartyVoterValue,
               OppositionSinglePartyVoterValue,

               PrintBool(SingleTotalCondition),
               PrintBool(CoalitionSingleCoalitionCondition),
               PrintBool(SingleOppositionOppositionCondition),

               CoalitionVoterValueRatio,
               OppositionVoterValueRatio,
               SinglePartyVoterValueRatio,

               CoalitionSinglePartyVoterValueRatio,
               OppositionSinglePartyVoterValueRatio,
               });
        }
        #endregion

        #region Insert Results To Record
            /// <summary>
            /// Executing Stablize Method on the context, and load the results into the record
            /// </summary>
            /// <param name="context">Voting Context</param>
        public void Stablize(VotingContext context)
        {
            if (context == null)
                return;

            Iterations = context.Stablize();
            PartiesAfter = context.Parties.Count;
        }
        /// <summary>
        /// Executing the FormatCoalition method on the context, and load the results from the outcome
        /// </summary>
        /// <param name="context">Voting Context</param>
        /// <returns>Coalition that resulted from the FormatCoalition Method</returns>
        public Coalition FormatCoalition(VotingContext context)
        {
            if (context == null)
                return null;

            int leaderIndex;
            bool minimalCardinals;

            var coalition = context.FormatCoalition(out leaderIndex, out minimalCardinals);

            LeaderIndex = leaderIndex;
            IsMininalCardinals = minimalCardinals;

            SinglePartyValue = context.SinglePartyValue;
            ProperCoalitions = context.ProperCoalitions.Count;
            CoreCoalitions = context.CoreCoalitions.Count;

            if (CoreCoalitions == 0)
                CoreCoalitions = double.NaN;

            if (coalition != null)
            {
                CoalitionVoters = coalition.Size;
                CoalitionParties = coalition.Parties.Count;

                /// Calculate Coalition & Opposition Values
                CoalitionValue = 0;
                OppositionValue = 0;

                foreach (var party in context.Parties)
                    if (coalition.Parties.Contains(party))
                        CoalitionValue += coalition.Values[party];
                    else
                        OppositionValue += coalition.Values[party];

                CoalitionSinglePartyValue = coalition.CoalitionSinglePartyValue;
                OppositionSinglePartyValue = coalition.OppositionSinglePartyValue;
            }
            else
            {
                CoalitionVoters = 0;
                CoalitionParties = 0;

                CoalitionValue = double.NaN;
                OppositionValue = double.NaN;

                CoalitionSinglePartyValue = double.NaN;
                OppositionSinglePartyValue = double.NaN;
            }

            return coalition;
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Calculates the ratio only if the coalition is not empty
        /// </summary>
        /// <param name="top">The Numerator of the Ratio</param>
        /// <param name="bottom">The Denominator of the Ratio</param>
        /// <returns>Ratio if coalition exists, else double.NaN</returns>
        protected double CoalitionRatio(double top, double bottom) => CoalitionVoters > 0 ? Ratio(top, bottom) : double.NaN;
        #endregion
    }
}