﻿using System.Collections.Generic;

namespace CoalitionResearch.Records
{
    /// <summary>
    /// Service Methods for the Records
    /// </summary>
    public static class RecordService
    {
        #region Serialization
        /// <summary>
        /// Executing Mode's Name
        /// </summary>
        public static string ValueMode =>
#if REGRET
            "Regret"
#else
            "Utility"
#endif
            ;

        /// <summary>
        /// Prints Boolean function
        /// </summary>
        /// <param name="value">Boolean Value</param>
        /// <returns>1 for True, 0 for False</returns>
        public static char PrintBool(bool value) => value ? '1' : '0';

        /// <summary>
        /// Gets a Name of a property, and adds space before each Capital Letter
        /// </summary>
        /// <param name="name">Property's Name</param>
        /// <returns>Property's Name with Spaces </returns>
        public static string PrintName(string name)
        {
            name = name.Replace("Value", ValueMode);

            var result = "";
            var isNotFirst = false;

            foreach (var c in name)
            {
                if (char.IsUpper(c))
                {
                    if (isNotFirst)
                        result += " " + c;
                    else
                    {
                        isNotFirst = true;
                        result += "" + c;
                    }
                }
                else
                    result += c;
            }

            return result;
        }

        /// <summary>
        /// Executing PrintName on a full list of names
        /// </summary>
        /// <param name="names">List of Names</param>
        /// <returns>List of Names with Spaces</returns>
        public static ICollection<string> PrintNames(ICollection<string> names)
        {
            if ((names?.Count ?? 0) <= 0)
                return null;

            var result = new List<string>();

            foreach (var name in names)
                result.Add(PrintName(name));

            return result;
        }
        #endregion

        #region Condition
        /// <summary>
        /// Conditional Name
        /// </summary>
        /// <param name="a">
        /// In Utility = Smaller
        /// In Regret = Bigger
        /// </param>
        /// <param name="b">
        /// In Utility = Bigger
        /// In Regret = Smaller
        /// </param>
        /// <returns></returns>
        public static string ConditionName(string a, string b)
        {
            var nameA = PrintName(a);
            var nameB = PrintName(b);

#if REGRET
            return $"{nameA} >= {nameB}";
#else
            return $"{nameB} >= {nameA}";
#endif
        }

        /// <summary>
        /// Conditional Value
        /// </summary>
        /// <param name="a">
        /// In Utility = Smaller
        /// In Regret = Bigger
        /// </param>
        /// <param name="b">
        /// In Utility = Bigger
        /// In Regret = Smaller
        /// </param>
        /// <returns></returns>
        public static bool ConditionValue(double a, double b)
        {
            if (double.IsNaN(a) || double.IsNaN(b))
                return true;

#if REGRET
            return a >= b;
#else
            return b >= a;
#endif
        }
        #endregion

        #region Ratios
        /// <summary>
        /// Gets the numerator and the denominator of a ratio, and returns the double value.
        /// if the denominator = 0, return double.NaN
        /// </summary>
        /// <param name="top"></param>
        /// <param name="bottom"></param>
        /// <returns></returns>
        public static double Ratio(double top, double bottom) => double.IsNaN(top) || double.IsNaN(bottom) ||
            bottom == 0 ? double.NaN : top / bottom;
        /// <summary>
        /// Overflow Check
        /// </summary>
        /// <param name="value">Checked Value</param>
        /// <returns>True if the value > 1</returns>
        public static bool CheckOverflow(double value) => double.IsNaN(value) ? false : value > 1;

        /// <summary>
        /// Check All Cases are true
        /// </summary>
        /// <param name="value">Ratio of all cases</param>
        /// <returns>True if the ratio = 1</returns>
        public static bool CheckIs1(double value) => double.IsNaN(value) ? false : value == 1;
        #endregion
    }
}
