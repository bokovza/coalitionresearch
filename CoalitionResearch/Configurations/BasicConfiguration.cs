﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CoalitionResearch.Core.Utilities;

namespace CoalitionResearch.Configurations
{
    /// <summary>
    /// Configuration file for basic simulation
    /// </summary>
    public class BasicConfiguration : Configuration
    {
        #region Properties
        /// <summary>
        /// The Denominator of the Prototypes' Noise Filter.
        /// For Example: if np = 0.1 => Base = 10 (because 0.1 = 1/10)
        /// </summary>
        public int BaseNoisePrototypes { get; set; } = 10;
        /// <summary>
        /// The Denominator of the Voters' Noise Filter.
        /// For Example: if nv = 0.05 => Base = 20 (because 0.05 = 1/20)
        /// </summary>
        public int BaseNoiseVoters { get; set; } = 10;

        /// <summary>
        /// The Numerator of the Prototypes' Noise Filter.
        /// For Example: if np = 0.1 => Base = 1 (because 0.1 = 1/10)
        /// </summary>
        public ISet<int> NoisePrototypes { get; set; } = new HashSet<int> { 0, 1, 2, 3, 4, 5 };

        /// <summary>
        /// The Numerator of the Voters' Noise Filter.
        /// For Example: if nv = 0.15 => Base = 3 (because 0.15 = 3/20)
        /// </summary>
        public ISet<int> NoiseVoters { get; set; } = new HashSet<int> { 0, 1, 2, 3, 4, 5 };
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        public BasicConfiguration()
        {
            AddInt(nameof(BaseNoisePrototypes));
            AddInt(nameof(BaseNoiseVoters));

            AddIntList(nameof(NoisePrototypes));
            AddIntList(nameof(NoiseVoters));
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adding a list of int to the lists of tasks
        /// </summary>
        /// <param name="property">Property's name</param>
        protected void AddIntList(string property)
        {
            ToSerialize.Add(property, new Task<string>(() => StringFunctions.CreateListString(Get<ICollection<int>>(property))));

            ToDeserialize.Add(property, s =>
            {
                var lst = new HashSet<int>();

                var values = s.Split(',');

                foreach (var value in values)
                {
                    int k;
                    if (int.TryParse(value, out k))
                        lst.Add(k);
                }

                Set(property, lst);
            });

        }
        #endregion
    }
}