﻿using System.Collections.Generic;

namespace CoalitionResearch.Configurations
{
    /// <summary>
    /// Configuration file of the importance issues simulation
    /// </summary>
    public class ImportantIssuesConfiguration : BasicConfiguration
    {
        #region Properties
        /// <summary>
        /// Amounts of important issues
        /// </summary>
        public ISet<int> ImportantIssues { get; set; } = new HashSet<int> {10, 30, 50, 70, 90};
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        public ImportantIssuesConfiguration()
        {
            AddIntList(nameof(ImportantIssues));
        }
        #endregion
    }
}