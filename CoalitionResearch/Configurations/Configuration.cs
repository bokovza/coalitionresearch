﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CoalitionResearch.Utilities;
using CoalitionResearch.Core.Utilities;
using CoalitionResearch.Core;

namespace CoalitionResearch.Configurations
{
    /// <summary>
    /// Configuration file
    /// </summary>
    public abstract class Configuration : IFileSerializable
    {
        #region Properties
        /// <summary>
        /// Amount of issues
        /// </summary>
        public int Issues { get; set; } = 100;
        /// <summary>
        /// Jump of the issues in the prototypes
        /// </summary>
        public int PrototypesJump { get; set; } = 10;
        /// <summary>
        /// Amount of parties
        /// </summary>
        public int Parties { get; set; } = 15;
        /// <summary>
        /// How much runs does we execute of a single combinations of values ?
        /// </summary>
        public int Runs { get; set; } = 100;
        /// <summary>
        /// Amount of voters
        /// </summary>
        public int Voters { get; set; } = 1000;
        /// <summary>
        /// The electoral threshold
        /// </summary>
        public double ElectoralThreshold { get; set; } = 0.05;
        /// <summary>
        /// Directory's name of the output files.
        /// </summary>
        public string OutputDirectory { get; set; } = "Outputs";

        /// <summary>
        /// The values of the Beta distribution
        /// </summary>
        public ICollection<Tuple<double, double>> BetaValues { get; set; } = new List<Tuple<double, double>>()
        {
            new Tuple<double, double>(1, 1),
            new Tuple<double, double>(2, 2),
            new Tuple<double, double>(3, 3),
            new Tuple<double, double>(5, 5),
            new Tuple<double, double>(10, 10)
        };
        #endregion

        #region Internal Members
        /// <summary>
        /// Functions to serialize the file
        /// </summary>
        protected IDictionary<string, Task<string>> ToSerialize { get; } = new Dictionary<string, Task<string>>();
        /// <summary>
        /// Functions to deserialize the file
        /// </summary>
        protected IDictionary<string, Action<string>> ToDeserialize { get; } = new Dictionary<string, Action<string>>();
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        protected Configuration()
        {
            AddInt(nameof(Issues));
            AddInt(nameof(PrototypesJump));
            AddInt(nameof(Runs));
            AddInt(nameof(Voters));
            AddInt(nameof(Parties));
            AddDouble(nameof(ElectoralThreshold));
            Add(nameof(OutputDirectory));

            ToSerialize.Add(nameof(BetaValues), new Task<string>(SerializeBeta));
            ToDeserialize.Add(nameof(BetaValues), DeserializeBeta);
        }

        /// <summary>
        /// Simple property's handlers (primitive)
        /// </summary>
        /// <param name="property">Name of property</param>
        protected void Add(string property)
        {
            ToSerialize.Add(property, new Task<string>(() => Get<string>(property)));
            ToDeserialize.Add(property, value => Set(property, value));
        }
        /// <summary>
        /// Int property's handlers
        /// </summary>
        /// <param name="property">Name of property</param>
        protected void AddInt(string property)
        {
            ToSerialize.Add(property, new Task<string>(() => Get<int>(property).ToString()));
            ToDeserialize.Add(property, value => Set(property, int.Parse(value)));
        }
        /// <summary>
        /// Bool property's handler
        /// </summary>
        /// <param name="property">Name of property</param>
        protected void AddBool(string property)
        {
            ToSerialize.Add(property, new Task<string>(() => Get<bool>(property).ToString()));
            ToDeserialize.Add(property, value => Set(property, bool.Parse(value)));
        }

        /// <summary>
        /// Double property's handlers
        /// </summary>
        /// <param name="property">Name of property</param>
        protected void AddDouble(string property)
        {
            ToSerialize.Add(property, new Task<string>(() => Get<double>(property).ToString(CultureInfo.CurrentCulture)));
            ToDeserialize.Add(property, value => Set(property, double.Parse(value)));
        }

        /// <summary>
        /// Serialization of beta property
        /// </summary>
        /// <returns>string of beta values</returns>
        private string SerializeBeta() => $"{StringFunctions.CreateListString(BetaValues.Select(tuple => $"{tuple.Item1},{tuple.Item2}"), '|')}\n";
        /// <summary>
        /// deserialization of beta property
        /// </summary>
        /// <param name="value">the string to serialize</param>
        private void DeserializeBeta(string value)
        {
            BetaValues.Clear();
            var values = value.Split('|');

            if (values.Length == 0)
                return;

            foreach (
                var splitting in
                    values.Where(pair => !string.IsNullOrEmpty(pair))
                        .Select(pair => pair.Split(','))
                        .Where(splitting => splitting.Length == 2))
            {
                double a, b;
                if (double.TryParse(splitting[0], out a) && double.TryParse(splitting[1], out b))
                    BetaValues.Add(new Tuple<double, double>(a, b));
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets the value of a property using reflection
        /// </summary>
        /// <typeparam name="T">Any type</typeparam>
        /// <param name="prop">Name of property</param>
        /// <returns>Value of the property</returns>
        protected T Get<T>(string prop) => (T)GetType().GetProperty(prop).GetValue(this, null);
        /// <summary>
        /// Sets the value of a property using reflection
        /// </summary>
        /// <param name="prop">Name of the property</param>
        /// <param name="value">New value of the property</param>
        protected void Set(string prop, object value) => GetType().GetProperty(prop).SetValue(this, value);
        #endregion

        #region Implementations
        /// <summary>
        /// Serialize of the configuration file
        /// </summary>
        /// <param name="filePath">Destination file</param>
        public void Serialize(string filePath)
        {
            using (var file = new StreamWriter(filePath, false))
                foreach (var task in ToSerialize)
                {
                    var t = task.Value;
                    t.Start();
                    t.Wait();

                    file.WriteLine($"{task.Key}={t.Result}");
                }
        }
        /// <summary>
        /// Deserialization of the configuration file
        /// </summary>
        /// <param name="filePath">Source file</param>
        public void Deserialize(string filePath)
        {
            using (var file = new StreamReader(filePath))
            {
                while (!file.EndOfStream)
                {
                    var line = file.ReadLine() ?? string.Empty;
                    var split = line.Split('=');

                    if (split.Length != 2)
                        continue;

                    Action<string> task;
                    if (ToDeserialize.TryGetValue(split[0], out task))
                        task?.Invoke(split[1]);
                }
            }
        }
        #endregion
    }
}