﻿# Instructions for running the code

* Open the properties of the project
  * Solution Explorer
  * Right Click **CoalitionResearch**
  * Choose **Properties** (Or press Alt+Enter)


## Simple Regret
* Choose **Application**
  * Startup Object "CoalitionResearch.Program"
* Choose **Build**
  * Conditional Compilation Symbol: "REGRET"
* Choose **Debug**
  * Working Directory: ~\CoalitionResearch\WorkingDirectory\
  * Command Line Arguments
    * Basic Run: basic basicConf.txt
    * Important Run: important importantConf.txt
    * The configurations files are located in **Executables** Folder

## Dividers
* Choose **Application**
  * Startup Object "CoalitionResearch.DividerProgram"
* Choose **Build**
  * Conditional Compilation Symbol: "REGRET"
* Choose **Debug**
  * Working Directory: ~\CoalitionResearch\WorkingDirectory\
  * Command Line Arguments
    * Run: basicConf.txt
    * The configurations files are located in **Executables** Folder

In the **Simulations** folder,
there's the **Division** folder,
and the main file that intrests you is **DivisionSimulator.cs**

The method that intrests you is **CreateParties()** (Line 251).
If you wanna to create 15 parties with duplications of the profiles in the parties, keep the code as is
* Hide lines 259 - 303
* Show lines 205 - 311

If you wanna create parties without 2 parties that have the same profile,
* Show lines 259 - 303
* Hide lines 205 - 311
