﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CoalitionResearch.Utilities
{
    /// <summary>
    /// CSV's File Writer
    /// </summary>
    public class CsvWriter : IDisposable
    {
        #region Property

        /// <summary>
        /// Basic Stream Writer
        /// </summary>
        public StreamWriter Writer { get; } 

        /// <summary>
        /// Delimiter of CSV's fields
        /// </summary>
        public char Delimiter { get; }

        #endregion Property

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="filePath">Destination File</param>
        /// <param name="delimiter">Delimiter (default ',')</param>
        public CsvWriter(string filePath, char delimiter = ',')
        {
            Delimiter = delimiter;
            Writer = new StreamWriter(filePath);
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Write the list to the file
        /// </summary>
        /// <param name="data">Data of a row in the file</param>
        public void Write<T>(ICollection<T> data)
        {
            if (data == null)
                return;

            var isNotBeginning = false;

            foreach (var obj in data)
            {
                if (isNotBeginning)
                    Writer.Write(Delimiter);
                else
                    isNotBeginning = true;

                Writer.Write(obj);
            }

            Writer.Write("\n");
        }

        #endregion Methods

        #region IDisposable

        /// <summary>
        /// Dispose the writer
        /// </summary>
        public void Dispose()
        {
            
            Writer?.Dispose();
        }

        #endregion IDisposable
    }
}