﻿using CoalitionResearch.Core.Profiles;
using System;

namespace CoalitionResearch.Utilities
{
    /// <summary>
    /// Prototypes builder
    /// </summary>
    public class Prototypes
    {
        #region Properties
        /// <summary>
        /// Output profiles
        /// </summary>
        public Profile[] Profiles { get; }
        /// <summary>
        /// Header of profiles
        /// </summary>
        public ProfileHeader Header { get; }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructors
        /// </summary>
        /// <param name="header">Profiles' Header</param>
        /// <param name="jump">How much values to jump at each step</param>
        public Prototypes(ProfileHeader header, int jump = 1, NoiseFilter noise = null)
        {
            if ((Header = header) == null)
                throw new ArgumentNullException(nameof(header));
            if (jump < 1)
                throw new ArgumentOutOfRangeException(nameof(jump), "Must be >= 1");
            if (Header.Size % jump != 0)
                throw new ArgumentException(nameof(jump), $"{Header.Size} can't be devided by {jump}");

            Profiles = new Profile[Header.Size / jump + 1];

            for (int index = 0; index < Profiles.Length; index++)
            {
                var changeIndex = index * jump;
                var profile = Profiles[index] = new Profile(Header);
                

                for (var j = 0; j < profile.Size; j++)
                    profile[j] = j < changeIndex ? 1 : -1;

                if (noise == null)
                    continue;

                noise.Reset();
                noise.Generate(profile);
            }
        }
        #endregion
    }
}
