﻿using CoalitionResearch.Core.Profiles;
using MathNet.Numerics.Distributions;
using System;

namespace CoalitionResearch.Utilities
{
    /// <summary>
    /// Filter for adding noise to the data of profile
    /// </summary>
    public class NoiseFilter
    {
        #region Properties
        /// <summary>
        /// Amount of values (denominator)
        /// </summary>
        public int Levels { get; }

        /// <summary>
        /// Amount of noise (numerator)
        /// </summary>
        public int NoiseLevels { get; }

        /// <summary>
        /// The generators of the numbers
        /// </summary>
        private DiscreteUniform Generator { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="noise">Amount of noise</param>
        /// <param name="levels">Amount of values (default 10)</param>
        public NoiseFilter(int noise, int levels = 10)
        {
            if ((NoiseLevels = noise) < 0)
                throw new ArgumentOutOfRangeException(nameof(NoiseLevels), "Must be Non-Negative.");
            if ((Levels = levels) < 1)
                throw new ArgumentOutOfRangeException(nameof(Levels), "Must be Bigger or Equal to 1.");
            if (NoiseLevels > Levels)
                throw new ArgumentException("Level of Noise must be Small or Equal to Total.");

            Reset();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Reset the file
        /// </summary>
        public void Reset() => Generator = new DiscreteUniform(1, Levels);
        /// <summary>
        /// Adding noise based on the generator
        /// </summary>
        /// <param name="value">The origin value</param>
        /// <returns>The value after adding noise</returns>
        public int Generate(int value) => Generator.Sample() <= NoiseLevels ? -1 * value : value;
        /// <summary>
        /// Add noise to a profile
        /// </summary>
        /// <param name="p">A profile</param>
        public void Generate(Profile p)
        {
            for (var i = 0; i < (p?.Size ?? -1); i++)
                p[i] = Generate(p[i]);
        }
        #endregion
    }
}
