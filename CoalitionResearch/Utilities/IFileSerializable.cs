﻿namespace CoalitionResearch.Utilities
{
    /// <summary>
    /// Interface for serialization of files
    /// </summary>
    public interface IFileSerializable
    {
        /// <summary>
        /// To Serialize a file
        /// </summary>
        /// <param name="filePath">Destination file</param>
        void Serialize(string filePath);
        /// <summary>
        /// To Deserialization a file
        /// </summary>
        /// <param name="filePath">Source file</param>
        void Deserialize(string filePath);
    }
}