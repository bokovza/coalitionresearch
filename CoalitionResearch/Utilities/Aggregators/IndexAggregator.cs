﻿namespace CoalitionResearch.Utilities.Aggregators
{
    /// <summary>
    /// Aggregator for the Leader Index's Property
    /// </summary>
    public class IndexAggregator : Aggregator<int>
    {
        #region Properties
        /// <summary>
        /// The aggregation sum
        /// </summary>
        public double Sum { get; private set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Aggregator's Name</param>
        public IndexAggregator(string name) : base(name)
        {
        }
        #endregion

        #region Overrides
        public override double Result => Counter == 0 ? -1 : Sum / Counter;
        public override void Add(int value)
        {
            if (value == -1)
                return;

            Sum += value;
            Counter++;
        }

        protected override void CompleteReset()
        {
            Sum = 0;
        }
        #endregion
    }
}
