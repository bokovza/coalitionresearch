﻿namespace CoalitionResearch.Utilities.Aggregators
{
    /// <summary>
    /// Counts all the values that are true - greats for conditions
    /// </summary>
    public class ConditionAggregator : Aggregator<bool>
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Aggregator's Name</param>
        public ConditionAggregator(string name) : base(name)
        {
        }

        #endregion

        #region Properties
        /// <summary>
        /// Counter for the True conditions
        /// </summary>
        public double TrueCounter { get; private set; }
        #endregion

        #region Implementation
        
        public override double Result => TrueCounter / Counter;

        public override void Add(bool value)
        {
            Counter++;

            if (value)
                TrueCounter++;
        }

        protected override void CompleteReset()
        {
            TrueCounter = 0;
        }
        #endregion
    }
}
