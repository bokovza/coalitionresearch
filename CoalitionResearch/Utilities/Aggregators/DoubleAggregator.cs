﻿namespace CoalitionResearch.Utilities.Aggregators
{
    /// <summary>
    /// Aggregator for Doubles
    /// </summary>
    public class DoubleAggregator : Aggregator<double>
    {
        #region Properties
        /// <summary>
        /// Default Value in case of 0 values that aggregates
        /// </summary>
        public double Default => double.NaN;
        /// <summary>
        /// Aggregator's Sum
        /// </summary>
        public double Sum { get; private set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Aggregator's Name</param>
        public DoubleAggregator(string name) : base(name)
        {
        }
        #endregion

        #region Overrides
        public override double Result => Counter == 0 ? Default : Sum / Counter;
        public override void Add(double value)
        {
            if (double.IsNaN(value))
                return;

            Counter++;
            Sum += value;
        }

        protected override void CompleteReset()
        {
            Sum = 0;
        }
        #endregion
    }
}
