﻿namespace CoalitionResearch.Utilities.Aggregators
{
    /// <summary>
    /// Interface of an Aggregator
    /// </summary>
    public interface IAggregator
    {
        /// <summary>
        /// Name of Aggregator
        /// </summary>
        string Name { get; }
        /// <summary>
        /// The Counter that counts amount of items
        /// </summary>
        int Counter { get; }
        /// <summary>
        /// The Result of the aggregator
        /// </summary>
        double Result { get; }
        /// <summary>
        /// Reset Aggregator
        /// </summary>
        void Reset();
    }
}
