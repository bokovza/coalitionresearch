﻿using static CoalitionResearch.Records.RecordService;

namespace CoalitionResearch.Utilities.Aggregators
{
    /// <summary>
    /// Abstract Aggregator
    /// </summary>
    /// <typeparam name="T">Type of values that the aggregator can read</typeparam>
    public abstract class Aggregator<T> : IAggregator
    {
        #region Properties
        /// <summary>
        /// Aggregator's Name
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// Amount of counted values
        /// </summary>
        public int Counter { get; protected set; } = 0;
        /// <summary>
        /// The result of the aggregator
        /// </summary>
        public abstract double Result { get; }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Aggregator's Name</param>
        protected Aggregator(string name)
        {
            Name = PrintName(name);
            Reset();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Reset Aggregator
        /// </summary>
        public void Reset()
        {
            Counter = 0;
            CompleteReset();
        }
        #endregion

        #region Abstract Methods
        /// <summary>
        /// Add a value in order to aggregate it
        /// </summary>
        /// <param name="value">Value to aggregate</param>
        public abstract void Add(T value);
        /// <summary>
        /// Internal Reset for the implemening classes for the aggregator
        /// </summary>
        protected abstract void CompleteReset();
        #endregion
    }
}
