﻿using System.Collections.Generic;
using System.Linq;

namespace CoalitionResearch.Utilities
{
    /// <summary>
    /// Additional LINQ Methods
    /// </summary>
    public static class LinqExpansion
    {
        #region Average
        /// <summary>
        /// Average of numbers, when if there's no numbers inside the list, return Default Value
        /// </summary>
        /// <param name="lst">List of Double Numbers</param>
        /// <param name="defaultValue">Default Value in case of an empty list</param>
        /// <returns></returns>
        public static double Average(IEnumerable<double> lst, double defaultValue = double.NaN)
             => (lst == null || lst.Count() == 0) ? defaultValue : lst.Average();
        /// <summary>
        /// Average of numbers, when if there's no numbers inside the list, return Default Value
        /// </summary>
        /// <param name="lst">List of Integers</param>
        /// <param name="defaultValue">Default Value in case of an empty list</param>
        /// <returns></returns>
        public static double Average(IEnumerable<int> lst, double defaultValue = double.NaN)
            => (lst == null || lst.Count() == 0) ? defaultValue : lst.Average();

        /// <summary>
        /// Averages of numbers, with filter condition.
        /// If there's no numbers in the list / all numbers doesn't pass the codition, return Default Value
        /// </summary>
        /// <param name="lst">List of Doubles</param>
        /// <param name="whereCondition">Condition</param>
        /// <param name="defaultValue">Default Value</param>
        /// <returns></returns>
        public static double Average(IEnumerable<double> lst, OutAction<double> whereCondition, double defaultValue = double.NaN)
        {
            if (lst == null || lst.Count() == 0)
                return defaultValue;
            if (whereCondition == null)
                return Average(lst, defaultValue);

            var tmp = lst.Where(item => whereCondition(item)).ToList();

            return tmp.Count() == 0 ? defaultValue : tmp.Average();
        }

        /// <summary>
        /// Averages of numbers, with filter condition.
        /// If there's no numbers in the list / all numbers doesn't pass the codition, return Default Value
        /// </summary>
        /// <param name="lst">List of Integers</param>
        /// <param name="whereCondition">Condition</param>
        /// <param name="defaultValue">Default Value</param>
        /// <returns></returns>
        public static double Average(IEnumerable<int> lst, OutAction<int> whereCondition, double defaultValue = double.NaN)
        {
            if (lst == null || lst.Count() == 0)
                return defaultValue;
            if (whereCondition == null)
                return Average(lst, defaultValue);

            var tmp = lst.Where(item => whereCondition(item)).ToList();

            return tmp.Count() == 0 ? defaultValue : tmp.Average();
        }

        /// <summary>
        /// Calculates the average of a list, without count NaN.
        /// Default Value: double.NaN
        /// </summary>
        /// <param name="lst">List of Doubles</param>
        /// <returns>Average / Double.NaN as Default Value</returns>
        public static double AverageWithoutNaN(IEnumerable<double> lst) =>
    Average(lst, new OutAction<double>((item) => !double.IsNaN(item)));
        #endregion

        #region Delegates
        /// <summary>
        /// Form of functions that gets a value and return True/False (for condition)
        /// </summary>
        /// <typeparam name="T">Checked Value</typeparam>
        /// <param name="obj">True if the condition occurs</param>
        /// <returns></returns>
        public delegate bool OutAction<T>(T obj);
        #endregion

        #region Ratio

        public static double Ratio<T>(IEnumerable<T> lst, OutAction<T> whereCondition)
        {
            int total;
            if (lst == null || (total = lst.Count()) == 0)
                return 0;

            var top = lst.Where(item => whereCondition(item)).Count();

            return (double)top / total;
        }
        public static double Ratio(IEnumerable<bool> lst) =>
            Ratio(lst, new OutAction<bool>((item) => item));
        #endregion

    }
}
