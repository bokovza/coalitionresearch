﻿using System;

namespace CoalitionResearch.Utilities
{
    /// <summary>
    /// Stores data for calculating the Rand Measure
    /// </summary>
    public class GroupImages
    {
        #region Properties
        /// <summary>
        /// Before Stabilization Image
        /// </summary>
        public int[] Image1 { get; }
        /// <summary>
        /// After Stabilization Image
        /// </summary>
        public int[] Image2 { get; }
        /// <summary>
        /// Amount of values for each image
        /// </summary>
        public int Size { get; }
        /// <summary>
        /// The Denominator of the measure
        /// </summary>
        public int RandMeasureSize => (Size * (Size - 1)) / 2;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="size">Amount of values for each image</param>
        public GroupImages(int size)
        {
            if ((Size = size) < 2)
                throw new ArgumentOutOfRangeException(nameof(size), "Must be > 1");

            Image1 = new int[Size];
            Image2 = new int[Size];
        }
        #endregion

        #region Methods
        /// <summary>
        /// The numerator of the measure
        /// </summary>
        public int IntRandMeasure
        {
            get
            {
                var count = 0;

                for (var i = 0; i < Size; i++)
                    for (var j = i + 1; j < Size; j++)
                        if ((Image1[i] == Image1[j]) == (Image2[i] == Image2[j]))
                            count++;

                return count;
            }
        }

        /// <summary>
        /// Rand Measure value
        /// </summary>
        public double RandMeasure => (double)IntRandMeasure / RandMeasureSize;
        #endregion
    }
}