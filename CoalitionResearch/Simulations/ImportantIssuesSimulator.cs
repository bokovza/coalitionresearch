﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoalitionResearch.Configurations;
using CoalitionResearch.Records;
using MathNet.Numerics.Distributions;
using CoalitionResearch.Utilities;

namespace CoalitionResearch.Simulations
{
    public class ImportantIssuesSimulator : Simulator<ImportantIssuesConfiguration, ImportantIssuesRecord>
    {
        #region Members
        /// <summary>
        /// Prototypes' Noise Filter
        /// </summary>
        private NoiseFilter _prototypeNoise;
        /// <summary>
        /// Voters' Noise Filter
        /// </summary>
        private NoiseFilter _voterNoise;
        /// <summary>
        /// Full Noise Filter (noise = 0.5) for Not Important Issues
        /// </summary>
        private readonly NoiseFilter _fullNoise;
        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="filePath">The Path to the Configurator File</param>
        public ImportantIssuesSimulator(string filePath) : base(filePath)
        {
            _fullNoise = new NoiseFilter(5);
        }
        #endregion

        #region Implementation

        protected override Prototypes CreatePrototypes() => new Prototypes(Header, Configuration.PrototypesJump, _prototypeNoise);

        protected override void CreateVoters()
        {
            var beta = new Beta(AggregationRecord.Alpha, AggregationRecord.Beta);
            var generator = new DiscreteUniform(0, AggregationRecord.Issues - 1);
            var important = new HashSet<int>();
            var i = 0;

            _voterNoise = new NoiseFilter(AggregationRecord.NoiseVoters, Configuration.BaseNoiseVoters);

            foreach (var voter in Context.Voters)
            {
                important.Clear();
                _fullNoise.Reset();
                _voterNoise.Reset();

                while (important.Count < AggregationRecord.ImportantIssues)
                    important.Add(generator.Sample());

                var id = Image.Image1[i] = (int)Math.Min(10, Math.Floor(11 * beta.Sample()));
                voter.Copy(Prototypes.Profiles[id]);
                var vProfile = voter.Profile;

                for (var k = 0; k < Header.Size; k++)
                    vProfile[k] = important.Contains(k) ? _voterNoise.Generate(vProfile[k]) : _fullNoise.Generate(vProfile[k]);

                i++;
                }
            }

        protected override void CreateParties()
        {
            var st = new HashSet<int>();
            var rand = new Random();

            while (st.Count < AggregationRecord.Parties)
                st.Add(rand.Next(Configuration.Voters));

            for (var i = 0; i < AggregationRecord.Parties; i++)
            {
                var voter = Context.Voters.ElementAt(st.ElementAt(i));
                var party = Context.Parties.ElementAt(i);

                party.SetFirstVoter(voter);
            }
        }

        protected override void GenerateRuns()
        {
            foreach (var np in Configuration.NoisePrototypes)
            {
                _prototypeNoise = new NoiseFilter(np, Configuration.BaseNoisePrototypes);
                AggregationRecord.NoisePrototypes = np;
                foreach (var nv in Configuration.NoiseVoters)
                {
                    _voterNoise = new NoiseFilter(nv, Configuration.BaseNoiseVoters);
                    AggregationRecord.NoiseVoters = nv;

                    foreach (var imp in Configuration.ImportantIssues)
                    {
                        AggregationRecord.ImportantIssues = imp;
                        ExecuteAggregation();
                    }
                }
            }
        }

        protected override string FileName => $"{base.FileName}_imp{AggregationRecord.ImportantIssues}_np{AggregationRecord.NoisePrototypes}_nv{AggregationRecord.NoiseVoters}";

        public override int IterationsCount
            => Configuration.NoisePrototypes.Count * Configuration.BaseNoiseVoters * Configuration.BetaValues.Count * Configuration.ImportantIssues.Count;
        #endregion
    }
}