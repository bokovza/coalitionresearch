﻿using CoalitionResearch.Core.Profiles;
using CoalitionResearch.Utilities.Aggregators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoalitionResearch.Simulations.Division
{
    public class DivisionAggregator : IAggregator
    {
        public int Counter
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string Name { get; }

        ICollection<int[]> _profiles;

        public double Result => _profiles.Count();

        public DivisionAggregator(string name)
        {
            Name = name;
            _profiles = new List<int[]>();
        }

        public void Reset()
        {
            _profiles.Clear();
        }

        public void Add(IEnumerable<int[]> profiles)
        {
            foreach(var profile in profiles)
            {
                if(!_profiles.Any(p => IsMatch(p, profile)))
                    _profiles.Add(profile);
            }
        }

        static bool IsMatch(int[] a, int[] b)
        {
            for (var i = 0; i < a.Length; i++)
                if (a[i] != b[i])
                    return false;

            return true;
        }
    }
}
