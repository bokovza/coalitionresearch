﻿using CoalitionResearch.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoalitionResearch.Simulations.Division
{
    public class Checker
    {
        readonly VotingContext _context;
        readonly ISet<int[]> _profiles;

        public int Count => _profiles?.Count ?? 0;

        public Checker(VotingContext context)
        {
            if((_context = context) == null)
                throw new ArgumentNullException(nameof(context));

            _profiles = new HashSet<int[]>();
        }

        public void Reset() => _profiles.Clear();

        public void Check()
        {
            foreach (var party in _context.Parties)
            {
                var profile = party.Profile.Data;

                if(!_profiles.Any(p => IsMatch(profile, p)))
                {
                    _profiles.Add(profile);
                }
            }
        }

        static bool IsMatch(int[] array, int[] other)
        {
            var couples = array.Zip(other, (v1, v2) => new Tuple<int, int>(v1, v2));
            return !couples.Any(couple => couple.Item1 != couple.Item2);
        }
    }
}
