﻿using CoalitionResearch.Utilities;
using CoalitionResearch.Utilities.Aggregators;
using System.Collections.Generic;
using static System.Math;

namespace CoalitionResearch.Simulations.Division
{
    public class DivisionRecord
    {
        #region Properties
        
        public int Issues { get; set; }
        public int Voters { get; set; }
        public int Parties { get; set; }
        public int Runs { get; set; }

        public double ElectoralThreshold { get; set; }

        public double Alpha { get; set; }
        public double Beta { get; set; }
        public double BetaDistributionVariance => Alpha * Beta / (Pow(Alpha + Beta, 2) * (Alpha + Beta + 1));

        public long Runtime { get; set; }

        public int NoisePrototypes { get; set; }
        public int NoiseVoters { get; set; }

        public int NoisePrototypesBase { get; set; }
        public int NoiseVotersBase { get; set; }

        public DivisionAggregator Divisions { get; set; } = new DivisionAggregator("Divisions");
        public DoubleAggregator RandMeasurement { get; } = new DoubleAggregator(nameof(RandMeasurement));

        #endregion

        #region Methods

        public void WriteHeader(CsvWriter writer)
        {
            var lst = new List<object>
            {
                nameof(Issues),
                nameof(Voters),
                nameof(Parties),
                nameof(Runs),
                nameof(ElectoralThreshold),
                nameof(Alpha),
                nameof(Beta),
                nameof(BetaDistributionVariance),
                nameof(NoisePrototypes),
                nameof(NoiseVoters),
                nameof(Divisions),
                nameof(RandMeasurement),
                nameof(Runtime)
            };

            writer.Write(lst);
        }

        public void WriteData(CsvWriter writer)
        {
            var lst = new List<object>
            {
                Issues,
                Voters,
                Parties,
                Runs,
                ElectoralThreshold,
                Alpha,
                Beta,
                BetaDistributionVariance,
                NoisePrototypes,
                NoiseVoters,
                Divisions.Result,
                RandMeasurement.Result,
                Runtime
            };

            writer.Write(lst);
        }

        public void Reset()
        {
            Runtime = 0;
            Divisions.Reset();
            RandMeasurement.Reset();
        }

        #endregion
    }
}
