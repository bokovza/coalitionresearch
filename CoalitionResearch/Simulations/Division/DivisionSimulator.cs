﻿using System;
using System.Diagnostics;
using System.IO;
using CoalitionResearch.Configurations;
using CoalitionResearch.Core;
using CoalitionResearch.Core.Profiles;
using CoalitionResearch.Records;
using CoalitionResearch.Utilities;
using MathNet.Numerics.Distributions;
using System.Linq;
using System.Collections.Generic;

namespace CoalitionResearch.Simulations.Division
{
    /// <summary>
    /// An automation driver for the simulation
    /// </summary>
    public class DivisionSimulator
    {
        #region Members
        /// <summary>
        /// Stopwatch for measuring the time for each run
        /// </summary>
        private readonly Stopwatch _stopwatch = new Stopwatch();
        /// <summary>
        /// The prototypes of the simulation
        /// </summary>
        private Prototypes Prototypes { get; set; }
        private NoiseFilter _prototypeNoise;
        private NoiseFilter _voterNoise;
        #endregion

        #region Properties
        /// <summary>
        /// The scheme of the profiles
        /// </summary>
        public ProfileHeader Header { get; private set; }
        /// <summary>
        /// Configuration file
        /// </summary>
        public BasicConfiguration Configuration { get; }
        /// <summary>
        /// The current simulation record
        /// </summary>
        protected DivisionRecord AggregationRecord { get; }

        Checker _divisions;

        /// <summary>
        /// The current run record
        /// </summary>
        protected DividerRunRecord RunRecord { get; }
        /// <summary>
        /// Aggregation file's writer
        /// </summary>
        protected CsvWriter AggregationFile { get; private set; }
        /// <summary>
        /// Voting context
        /// </summary>
        protected VotingContext Context { get; set; }
        protected GroupImages Image { get; }
        #endregion

        #region Constructors
        public DivisionSimulator(string filePath)
        {
            Configuration = new BasicConfiguration();
            Configuration.Deserialize(filePath);

            AggregationRecord = new DivisionRecord();
            RunRecord = new DividerRunRecord();

            Image = new GroupImages(Configuration.Voters);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Full execution of the simulation
        /// </summary>
        public void Execute()
        {
            CheckConfiguration();

            Header = new ProfileHeader(Configuration.Issues);
            Context = new VotingContext(Header, Configuration.Voters, Configuration.Parties)
            {
                ElectoralThreshold = Configuration.ElectoralThreshold
            };

            AggregationRecord.Runs = Configuration.Runs;
            AggregationRecord.Parties = Configuration.Parties;
            AggregationRecord.Voters = Configuration.Voters;
            AggregationRecord.Issues = Configuration.Issues;
            AggregationRecord.ElectoralThreshold = Configuration.ElectoralThreshold;

            Directory.CreateDirectory(Configuration.OutputDirectory);

            using (AggregationFile = new CsvWriter($"{Configuration.OutputDirectory}/aggregation.csv"))
            {
                AggregationRecord.WriteHeader(AggregationFile);

                foreach (var bValues in Configuration.BetaValues)
                {
                    AggregationRecord.Alpha = bValues.Item1;
                    AggregationRecord.Beta = bValues.Item2;

                    GenerateRuns();
                }
            }
        }
        /// <summary>
        /// Execute a single run
        /// </summary>
        protected void ExecuteRun()
        {
            _stopwatch.Reset();
            _stopwatch.Start();

            CreateParties();
            Context.GenerateMajor();

            RunRecord.Stablize(Context);

            _stopwatch.Stop();

            {
                var i = 0;
                foreach (var voter in Context.Voters)
                {
                    Image.Image2[i] = voter.Party?.Id ?? -1;
                    i++;
                }
            }

            AggregationRecord.RandMeasurement.Add(RunRecord.RandMeasurement = Image.RandMeasure);

            RunRecord.Runtime = _stopwatch.ElapsedMilliseconds;
        }

        /// <summary>
        /// Execute multiple runs with the same parameters
        /// </summary>
        protected void ExecuteAggregation()
        {
            RunRecord.Voters = Configuration.Voters;

            using (var sessionFile = new CsvWriter($"{Configuration.OutputDirectory}/{FileName}.csv"))
            {
                RunRecord.WriteHeader(sessionFile);

                AggregationRecord.Reset();

                Prototypes = CreatePrototypes();
                CreateVoters();

                for (var i = 1; i <= Configuration.Runs; i++)
                {
                    RunRecord.Id = i;

                    Context.Reset();

                    Console.Write($"{'\r'}{FileName} [{RunRecord.Id:D3}] ({AggregationRecord.Runtime:D15})");

                    ExecuteRun();

                    AggregationRecord.Runtime += RunRecord.Runtime;
                    AggregationRecord.Divisions.Add(
                        Context.Parties.Select(party => party.Profile.Data));
                    RunRecord.Write(sessionFile);
                }

            }

            AggregationRecord.WriteData(AggregationFile);

            Console.WriteLine($"{'\r'}{FileName} [-D-] !{AggregationRecord.Runtime:D15}! {AggregationRecord.Divisions.Result}");
        }
        #endregion

        #region Virtual Methods
        /// <summary>
        /// The current file name that the simulator is working on at the moment
        /// </summary>
        protected virtual string FileName => $"i{AggregationRecord.Issues}_p{AggregationRecord.Parties}_v{AggregationRecord.Voters}_a{AggregationRecord.Alpha}_b{AggregationRecord.Beta}_pr{AggregationRecord.ElectoralThreshold}_np{AggregationRecord.NoisePrototypes}_nv{AggregationRecord.NoiseVoters}";
        /// <summary>
        /// Checking configuration file
        /// </summary>
        protected virtual void CheckConfiguration()
        {
            if (Configuration.Issues < 1)
                throw new ArgumentOutOfRangeException(nameof(Configuration.Issues), "Must be > 0");
            if (Configuration.Runs < 1)
                throw new ArgumentOutOfRangeException(nameof(Configuration.Runs), "Must be > 0");
            if (Configuration.Parties < 1)
                throw new ArgumentOutOfRangeException(nameof(Configuration.Parties), "Must be > 0");
            if (Configuration.Voters < 2)
                throw new ArgumentOutOfRangeException(nameof(Configuration.Voters), "Must be > 1");
            if (Configuration.OutputDirectory == string.Empty)
                throw new ArgumentException("Directory is incorrent", nameof(Configuration.OutputDirectory));
            if (Configuration.ElectoralThreshold < 0 || Configuration.ElectoralThreshold > 1)
                throw new ArgumentOutOfRangeException(nameof(Configuration.ElectoralThreshold), "Must be between 0 and 1");
            if (Configuration.BetaValues.Count == 0)
                throw new ArgumentException("Must include Values", nameof(Configuration.BetaValues));
            if (Configuration.PrototypesJump < 1)
                throw new ArgumentOutOfRangeException(nameof(Configuration.PrototypesJump), "Must be >= 1");
            if (Configuration.Issues % Configuration.PrototypesJump != 0)
                throw new ArgumentException($"{Configuration.Issues} can't be devided by {Configuration.PrototypesJump}", nameof(Configuration.PrototypesJump));

            foreach (var values in Configuration.BetaValues)
            {
                if (values.Item1 <= 0)
                    throw new ArgumentOutOfRangeException(nameof(Configuration.BetaValues), "Alpha must be > 0");
                if (values.Item2 <= 0)
                    throw new ArgumentOutOfRangeException(nameof(Configuration.BetaValues), "Beta must be > 0");
            }
        }
        #endregion

        #region Abstract Methods
        /// <summary>
        /// Creating the prototypes
        /// </summary>
        /// <returns></returns>
        private Prototypes CreatePrototypes()
        {
            return new Prototypes(Header, Configuration.PrototypesJump, _prototypeNoise);
        }
        /// <summary>
        /// Creating the voters
        /// </summary>
        private void CreateVoters()
        {
            var beta = new Beta(AggregationRecord.Alpha, AggregationRecord.Beta);
            var profile = new Profile(Header);
            var i = 0;

            foreach (var voter in Context.Voters)
            {
                var id = Image.Image1[i] = (int)Math.Min(10, Math.Floor(11 * beta.Sample()));
                voter.Copy(Prototypes.Profiles[id]);
                _voterNoise.Generate(voter.Profile);

                i++;
                _voterNoise.Reset();
            }
        }
        /// <summary>
        /// Creating the parties
        /// </summary>
        void CreateParties()
        {
            var st = new HashSet<int>();
            var chooser = new DiscreteUniform(0, Configuration.Voters - 1);

            while (st.Count < AggregationRecord.Parties)
                st.Add(chooser.Sample());

            //try
            //{
            //    while (st.Count < AggregationRecord.Parties)
            //        st.Add(chooser.Sample());

            //    var leaders = new HashSet<Voter>();

            //    foreach (var idx in st)
            //    {
            //        leaders.Add(Context.Voters.ElementAt(idx));
            //    }

            //    var distinctLeaders = new HashSet<Voter>();

            //    foreach (var leader in leaders)
            //    {
            //        if (!distinctLeaders.Any(voter => voter.Profile.Equals(leader.Profile)))
            //        {
            //            distinctLeaders.Add(leader);
            //        }
            //    }

            //    var toDelete = new HashSet<Party>();
            //    var i = 0;
            //    foreach (var party in Context.Parties)
            //    {
            //        if (i < distinctLeaders.Count)
            //        {
            //            var leader = distinctLeaders.ElementAt(i);
            //            party.SetFirstVoter(leader);
            //            i++;
            //        }
            //        else
            //        {
            //            toDelete.Add(party);
            //        }
            //    }

            //    foreach (var party in toDelete)
            //        Context.Parties.Remove(party);
            //}
            //catch (Exception)
            //{
            //    Console.WriteLine("Error");
            //}

            for (int idx = 0; idx < AggregationRecord.Parties; idx++)
            {
                var voter = Context.Voters.ElementAt(st.ElementAt(idx));
                var party = Context.Parties.ElementAt(idx);

                party.SetFirstVoter(voter);
            }
        }
        /// <summary>
        /// Executing the runs
        /// </summary>
        void GenerateRuns()
        {
            AggregationRecord.NoisePrototypesBase = Configuration.BaseNoisePrototypes;
            AggregationRecord.NoiseVotersBase = Configuration.BaseNoiseVoters;

            _divisions = new Checker(Context);

            foreach (var np in Configuration.NoisePrototypes)
            {
                _prototypeNoise = new NoiseFilter(np, AggregationRecord.NoisePrototypesBase);
                AggregationRecord.NoisePrototypes = np;

                foreach (var nv in Configuration.NoiseVoters)
                {
                    _voterNoise = new NoiseFilter(nv, AggregationRecord.NoiseVotersBase);
                    AggregationRecord.NoiseVoters = nv;

                    ExecuteAggregation();
                }
            }
        }
        #endregion
    }
}