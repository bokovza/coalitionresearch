﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoalitionResearch.Configurations;
using CoalitionResearch.Core.Profiles;
using CoalitionResearch.Records;
using MathNet.Numerics.Distributions;
using CoalitionResearch.Utilities;

namespace CoalitionResearch.Simulations
{
    public class BasicSimulator : Simulator<BasicConfiguration, BasicSimulationRecord>
    {
        #region Members
        private NoiseFilter _prototypeNoise;
        private NoiseFilter _voterNoise;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="filePath">The Path to the Configurator File</param>
        public BasicSimulator(string filePath) : base(filePath)
        {
        }

        #endregion

        #region Methods
        /// <summary>
        /// The Name of the file that currently the program is running on it.
        /// </summary>
        protected override string FileName => $"{base.FileName}_np{AggregationRecord.NoisePrototypes}_nv{AggregationRecord.NoiseVoters}";

        #endregion

        #region Abstract Implementation

        protected override Prototypes CreatePrototypes() => new Prototypes(Header, Configuration.PrototypesJump, _prototypeNoise);

        protected override void CreateVoters()
        {
            var beta = new Beta(AggregationRecord.Alpha, AggregationRecord.Beta);
            var profile = new Profile(Header);
            var i = 0;

            foreach (var voter in Context.Voters)
            {
                var id = Image.Image1[i] = (int)Math.Min(10, Math.Floor(11 * beta.Sample()));
                voter.Copy(Prototypes.Profiles[id]);
                _voterNoise.Generate(voter.Profile);

                i++;
                _voterNoise.Reset();
            }
        }

        protected override void CreateParties()
        {
            var st = new HashSet<int>();
            var chooser = new DiscreteUniform(0, Configuration.Voters - 1);

            while (st.Count < AggregationRecord.Parties)
                st.Add(chooser.Sample());

            for (var i = 0; i < AggregationRecord.Parties; i++)
            {
                var voter = Context.Voters.ElementAt(st.ElementAt(i));
                var party = Context.Parties.ElementAt(i);

                party.SetFirstVoter(voter);
            }
        }

        protected override void GenerateRuns()
        {
            AggregationRecord.NoisePrototypesBase = Configuration.BaseNoisePrototypes;
            AggregationRecord.NoiseVotersBase = Configuration.BaseNoiseVoters;

            foreach (var np in Configuration.NoisePrototypes)
            {
                _prototypeNoise = new NoiseFilter(np, AggregationRecord.NoisePrototypesBase);
                AggregationRecord.NoisePrototypes = np;
                
                foreach (var nv in Configuration.NoiseVoters)
                {
                    _voterNoise = new NoiseFilter(nv, AggregationRecord.NoiseVotersBase);
                    AggregationRecord.NoiseVoters = nv;

                    ExecuteAggregation();
                }
            }
        }

        public override int IterationsCount
            => Configuration.NoisePrototypes.Count*Configuration.BaseNoiseVoters*Configuration.BetaValues.Count;

        #endregion
    }
}