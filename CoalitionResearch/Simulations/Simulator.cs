﻿using System;
using System.Diagnostics;
using System.IO;
using CoalitionResearch.Configurations;
using CoalitionResearch.Core;
using CoalitionResearch.Core.Profiles;
using CoalitionResearch.Records;
using CoalitionResearch.Utilities;

namespace CoalitionResearch.Simulations
{
    /// <summary>
    /// An automation driver for the simulation
    /// </summary>
    /// <typeparam name="TConf">Type of configuration file</typeparam>
    /// <typeparam name="TMulti">Type of simulation record</typeparam>
    public abstract class Simulator<TConf, TMulti>
        where TConf : Configuration, new()
        where TMulti : SimulationRecord, new()
    {
        #region Members
        /// <summary>
        /// Stopwatch for measuring the time for each run
        /// </summary>
        private readonly Stopwatch _stopwatch = new Stopwatch();
        /// <summary>
        /// The prototypes of the simulation
        /// </summary>
        protected Prototypes Prototypes { get; set; }
        #endregion

        #region Properties
        /// <summary>
        /// The scheme of the profiles
        /// </summary>
        public ProfileHeader Header { get; private set; }
        /// <summary>
        /// Configuration file
        /// </summary>
        public TConf Configuration { get; }
        /// <summary>
        /// The current simulation record
        /// </summary>
        protected TMulti AggregationRecord { get; }
        /// <summary>
        /// The current run record
        /// </summary>
        protected RunRecord RunRecord { get; }
        /// <summary>
        /// Aggregation file's writer
        /// </summary>
        protected CsvWriter AggregationFile { get; private set; }
        /// <summary>
        /// Voting context
        /// </summary>
        protected VotingContext Context { get; set; }
        /// <summary>
        /// Images for Rand measure
        /// </summary>
        protected GroupImages Image { get; }
        /// <summary>
        /// Amount of iterations at total
        /// </summary>
        public abstract int IterationsCount { get; }
        /// <summary>
        /// Amount of runs at total
        /// </summary>
        public int RunsCount => Configuration.Runs * IterationsCount;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="filePath">File path of Configuration file</param>
        protected Simulator(string filePath)
        {
            Configuration = new TConf();
            Configuration.Deserialize(filePath);

            AggregationRecord = new TMulti();
            RunRecord = new RunRecord();

            Image = new GroupImages(Configuration.Voters);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Full execution of the simulation
        /// </summary>
        public void Execute()
        {
            CheckConfiguration();

            Header = new ProfileHeader(Configuration.Issues);
            Context = new VotingContext(Header, Configuration.Voters, Configuration.Parties)
            {
                ElectoralThreshold = Configuration.ElectoralThreshold
            };

            AggregationRecord.Runs = Configuration.Runs;
            AggregationRecord.Parties = Configuration.Parties;
            AggregationRecord.Voters = Configuration.Voters;
            AggregationRecord.Issues = Configuration.Issues;
            AggregationRecord.ElectoralThreshold = Configuration.ElectoralThreshold;

            Directory.CreateDirectory(Configuration.OutputDirectory);

            using (AggregationFile = new CsvWriter($"{Configuration.OutputDirectory}/aggregation.csv"))
            {
                AggregationRecord.WriteHeader(AggregationFile);

                foreach (var bValues in Configuration.BetaValues)
                {
                    AggregationRecord.Alpha = bValues.Item1;
                    AggregationRecord.Beta = bValues.Item2;

                    GenerateRuns();
                }
            }
        }
        /// <summary>
        /// Execute a single run
        /// </summary>
        protected void ExecuteRun()
        {
            _stopwatch.Reset();
            _stopwatch.Start();

            Prototypes = CreatePrototypes();
            CreateVoters();
            CreateParties();
            Context.GenerateMajor();

            RunRecord.Stablize(Context);

            {
                var i = 0;
                foreach (var voter in Context.Voters)
                {
                    Image.Image2[i] = voter.Party?.Id ?? -1;
                    i++;
                }
            }

            AggregationRecord.RandMeasurement.Add(RunRecord.RandMeasurement = Image.RandMeasure);
           
            RunRecord.FormatCoalition(Context);
            AggregationRecord.Accumulate(RunRecord);

            _stopwatch.Stop();

            AggregationRecord.Runtime += RunRecord.Runtime = _stopwatch.ElapsedMilliseconds;
        }
        /// <summary>
        /// Execute multiple runs with the same parameters
        /// </summary>
        protected void ExecuteAggregation()
        {
            
            using (var sessionFile = new CsvWriter($"{Configuration.OutputDirectory}/{FileName}.csv"))
            {

                RunRecord.WriteHeader(sessionFile);
                RunRecord.Voters = Configuration.Voters;
                AggregationRecord.Reset();

                for (var i = 1; i <= Configuration.Runs; i++)
                {
                    RunRecord.Id = i;
                    
                    Console.Write($"{'\r'}{FileName} [{RunRecord.Id:D3}] ({AggregationRecord.Runtime:D15})");

                    Context.Reset();
                    ExecuteRun();
                    RunRecord.Write(sessionFile);
                }

                AggregationRecord.Write(AggregationFile);
            }
            Console.WriteLine($"{'\r'}{FileName} [-D-] !{AggregationRecord.Runtime:D15}!");
        }
        #endregion

        #region Virtual Methods
        /// <summary>
        /// The current file name that the simulator is working on at the moment
        /// </summary>
        protected virtual string FileName => $"i{AggregationRecord.Issues}_p{AggregationRecord.Parties}_v{AggregationRecord.Voters}_a{AggregationRecord.Alpha}_b{AggregationRecord.Beta}_pr{AggregationRecord.ElectoralThreshold}";
        /// <summary>
        /// Checking configuration file
        /// </summary>
        protected virtual void CheckConfiguration()
        {
            if (Configuration.Issues < 1)
                throw new ArgumentOutOfRangeException(nameof(Configuration.Issues), "Must be > 0");
            if (Configuration.Runs < 1)
                throw new ArgumentOutOfRangeException(nameof(Configuration.Runs), "Must be > 0");
            if (Configuration.Parties < 1)
                throw new ArgumentOutOfRangeException(nameof(Configuration.Parties), "Must be > 0");
            if (Configuration.Voters < 2)
                throw new ArgumentOutOfRangeException(nameof(Configuration.Voters), "Must be > 1");
            if (Configuration.OutputDirectory == string.Empty)
                throw new ArgumentException("Directory is incorrent", nameof(Configuration.OutputDirectory));
            if (Configuration.ElectoralThreshold < 0 || Configuration.ElectoralThreshold > 1)
                throw new ArgumentOutOfRangeException(nameof(Configuration.ElectoralThreshold), "Must be between 0 and 1");
            if (Configuration.BetaValues.Count == 0)
                throw new ArgumentException("Must include Values", nameof(Configuration.BetaValues));
            if (Configuration.PrototypesJump < 1)
                throw new ArgumentOutOfRangeException(nameof(Configuration.PrototypesJump), "Must be >= 1");
            if (Configuration.Issues % Configuration.PrototypesJump != 0)
                throw new ArgumentException($"{Configuration.Issues} can't be devided by {Configuration.PrototypesJump}", nameof(Configuration.PrototypesJump));

            foreach (var values in Configuration.BetaValues)
            {
                if (values.Item1 <= 0)
                    throw new ArgumentOutOfRangeException(nameof(Configuration.BetaValues), "Alpha must be > 0");
                if (values.Item2 <= 0)
                    throw new ArgumentOutOfRangeException(nameof(Configuration.BetaValues), "Beta must be > 0");
            }
        }
        #endregion

        #region Abstract Methods
        /// <summary>
        /// Creating the prototypes
        /// </summary>
        /// <returns></returns>
        protected abstract Prototypes CreatePrototypes();
        /// <summary>
        /// Creating the voters
        /// </summary>
        protected abstract void CreateVoters();
        /// <summary>
        /// Creating the parties
        /// </summary>
        protected abstract void CreateParties();
        /// <summary>
        /// Executing the runs
        /// </summary>
        protected abstract void GenerateRuns();
        #endregion
    }
}