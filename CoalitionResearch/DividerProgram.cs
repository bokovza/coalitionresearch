﻿using CoalitionResearch.Simulations.Division;
using System;
using static System.Console;

namespace CoalitionResearch
{
    public class DividerProgram
    {
        public static void Main(string[] args)
        {
            if (args == null || args.Length != 1)
            {
                WriteLine("Only send 1 arguement : [configuration file path]");
                return;
            }

            var filePath = args[0];

            try
            {
                var simulator = new DivisionSimulator(filePath);
                simulator.Execute();
            }
            catch (Exception e)
            {
                WriteException(e);
                throw;
            }
        }

        static void WriteException(Exception e)
        {
            WriteLine($"Exception");
            WriteLine("==========");

            var p = e;

            while (p != null)
            {
                WriteLine($"Type: {e.GetType().Name}");
                WriteLine($"Message: {e.Message}");
                WriteLine($"Trace: ");
                WriteLine(e.StackTrace);

                p = p.InnerException;

                if (p != null)
                    WriteLine("==================================================================");
            }
        }
    }
}