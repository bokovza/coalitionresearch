﻿using CoalitionResearch.Core;
using CoalitionResearch.Core.Profiles;
using MathNet.Numerics.Distributions;
using System.Collections.Generic;
using System.Linq;

namespace CoalitionResearch
{
    class SearcherProgram
    {
        public static void Main(string[] args)
        {
            var isNoCore = true;

            while (isNoCore)
            {
                var context = new VotingContext(new ProfileHeader(3), 15, 3);
                var generator = new DiscreteUniform(1, 101);
                var partyGenerator = new DiscreteUniform(0, context.Voters.Length + 1);

                var strPartiesBefore = "";
                var strVoters = "";
                var strPartiesAfter = "";

                foreach (var voter in context.Voters)
                {
                    for (var i = 0; i < context.Header.Size; i++)
                        voter[i] = 1 + -2 * (generator.Sample() % 2);
                }

                var st = new HashSet<int>();
                while(st.Count < context.Parties.Count)
                    st.Add(partyGenerator.Sample());

                for (var i = 0; i < context.Parties.Count; i++)
                    context.Parties.ElementAt(i).SetFirstVoter(context.Voters.ElementAt(i));

                foreach (var party in context.Parties)
                    strPartiesBefore += $"{party.ToString()} ({party.Size})\r\n";

                context.Stablize();


                int leaderIndex;
                bool minCardinals;
                if(context.FormatCoalition(out leaderIndex, out minCardinals) == null)
                {
                    isNoCore = false;
                    System.Console.WriteLine("Done !");

                    foreach (var voter in context.Voters)
                        strVoters += $"{voter.ToString()} ({voter?.Party.Id ?? -1})\r\n";
                    foreach (var party in context.Parties)
                        strPartiesAfter += $"{party.ToString()} ({party.Size})\r\n";

                    System.Console.WriteLine("Voters");
                    System.Console.WriteLine(strVoters);
                    System.Console.WriteLine("===================");
                    System.Console.WriteLine("Before");
                    System.Console.WriteLine(strPartiesBefore);
                    System.Console.WriteLine("===================");
                    System.Console.WriteLine("After");
                    System.Console.WriteLine(strPartiesAfter);

                    var strCoalitions = "";
                    foreach(var coalition in context.ProperCoalitions)
                    {
                        strCoalitions += coalition.ToString() + "==>";

                        foreach(var util in coalition.Values)
                        {
                            strCoalitions += $" p{util.Key.Id}={util.Value}";
                        }

                        strCoalitions += "\r\n";
                    }

                    System.Console.WriteLine(strCoalitions);
                }
                else
                {
                    System.Console.WriteLine("Failed !");
                }
                
            }

            System.Console.ReadKey();
        }
    }
}
