﻿using System;
using CoalitionResearch.Simulations;

namespace CoalitionResearch
{
    /// <summary>
    /// The main code that runs at start
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Prints an error message
        /// </summary>
        protected static void PrintUsage()
        {
            Console.WriteLine("Usage: [Type] [Configuration File Path]");
            Console.WriteLine("=======================================");
            Console.WriteLine("Types: (not case sensitive)");
            Console.WriteLine("1. Basic");
            Console.WriteLine("2. Importance");
        }

        public static void Main(string[] args)
        {
            if (args == null || args.Length % 2 != 0)
                PrintUsage();

            for (var i = 0; i < args.Length; i += 2)
            {
                // ReSharper disable once PossibleNullReferenceException
                var type = args[i].ToLower();
                var filePath = args[i + 1].ToLower();

#if !DEBUG
                try
                {
#endif

                // ReSharper disable once ConvertIfStatementToSwitchStatement
                if (type == "basic")
                {
                    var sim = new BasicSimulator(filePath);
                    sim.Execute();
                }
                else if (type == "importance")
                {
                    var sim = new ImportantIssuesSimulator(filePath);
                    sim.Execute();
                }
                else
                    PrintUsage();
#if !DEBUG
            }
                catch (Exception e)
                {
                    Console.WriteLine($"Exception was thrown : {e.Message}");
                }
#endif
            }
        }
    }
}
