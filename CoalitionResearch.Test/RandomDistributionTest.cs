﻿using System.IO;
using MathNet.Numerics.Distributions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static System.Math;

namespace CoalitionResearch.Test
{
    [TestClass]
    public class RandomDistributionTest
    {
        [TestMethod]
        public void CheckBetaDistribution()
        {
            using (var file = new StreamWriter("distribution2.csv"))
            {
                file.WriteLine("alpha,beta,1,2,3,4,5,6,7,8,9,10,11");

                RunBeta2(file, 1, 1);
                RunBeta2(file, 1.2, 1.2);
                RunBeta2(file, 2, 2);
                RunBeta2(file, 5, 5);
                RunBeta2(file, 10, 10);
            }
        }

        private void RunBeta(StreamWriter writer, double alpha, double beta, int runs = 1000000)
        {
            var dist = new Beta(alpha, beta);
            var array = new int[11];

            for (var i = 1; i <= runs; i++)
                array[(int)Min(10, Floor(11 * dist.Sample()))]++;

            var str = "";
            for (var i = 0; i < array.Length; i++)
                str += (i > 0 ? "," : "") + array[i];

            writer?.WriteLine($"{alpha},{beta},{str}");
        }

        private void RunBeta2(StreamWriter writer, double alpha, double beta, int runs = 1000000)
        {
            var dist = new Beta(alpha, beta);
            var array = new int[11];
            var tuple = new double[11];

            for (var i = 0; i < 11; i++)
                // ReSharper disable once PossibleLossOfFraction
                tuple[i] = (double)(i + 1)/11;

            for (var i = 1; i <= runs; i++)
            {
                var sample = dist.Sample();
                for(var k =0; k < tuple.Length; k++)
                    if (sample <= tuple[k])
                    {
                        array[k]++;
                        break;
                    }
            }

            var str = "";
            for (var i = 0; i < array.Length; i++)
                str += (i > 0 ? "," : "") + array[i];

            writer?.WriteLine($"{alpha},{beta},{str}");
        }
    }
}