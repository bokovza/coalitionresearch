﻿using System.Linq;
using CoalitionResearch.Configurations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoalitionResearch.Test.Files
{
    [TestClass]
    public class ConfigurationTest
    {
        private const string BasicFilePath = "basic.txt";
        private const string ImportantIssuesFilePath = "important.txt";

        private static void CheckConfigurations(Configuration conf, Configuration validate)
        {
            Assert.AreEqual(validate.Issues, conf.Issues);
            Assert.AreEqual(validate.Parties, conf.Parties);
            Assert.AreEqual(validate.PrototypesJump, conf.PrototypesJump);
            Assert.AreEqual(validate.ElectoralThreshold, conf.ElectoralThreshold);
            Assert.AreEqual(validate.Voters, conf.Voters);
            Assert.AreEqual(validate.OutputDirectory, conf.OutputDirectory);
            Assert.AreEqual(validate.BetaValues?.Count ?? -1, conf.BetaValues?.Count ?? -1);
            
            foreach (var betaValue in conf.BetaValues)
                Assert.AreEqual(
                    validate.BetaValues.Count(item => item.Item1 == betaValue.Item1 && item.Item2 == betaValue.Item2), 1);
        }

        private static void CheckBasicConfigurations(BasicConfiguration conf, BasicConfiguration validate)
        {
            CheckConfigurations(conf, validate);
            Assert.AreEqual(validate.BaseNoisePrototypes, conf.BaseNoisePrototypes);
            Assert.AreEqual(validate.BaseNoiseVoters, conf.BaseNoiseVoters);

            foreach (var np in conf.NoisePrototypes)
                Assert.AreEqual(
                    validate.NoisePrototypes.Count(item => item == np), 1);

            foreach (var nv in conf.NoiseVoters)
                Assert.AreEqual(
                    validate.NoiseVoters.Count(item => item == nv), 1);
        }

        private static void CheckImportantIssuesConfigurations(ImportantIssuesConfiguration conf,
            ImportantIssuesConfiguration validate)
        {
            CheckBasicConfigurations(conf, validate);

            foreach (var importantIssue in conf.ImportantIssues)
                Assert.AreEqual(1, validate.ImportantIssues.Count(item => item == importantIssue));
        }

        private static void Check(Configuration conf, Configuration validate)
        {
            var confA = conf as BasicConfiguration;
            var validateA = validate as BasicConfiguration;

            if (confA != null && validateA != null)
            {
                var confB = conf as ImportantIssuesConfiguration;
                var validateB = validate as ImportantIssuesConfiguration;

                if (confB != null && validateB != null)
                    CheckImportantIssuesConfigurations(confB, validateB);
                else
                    CheckBasicConfigurations(confA, validateA);
            }
            else
                CheckConfigurations(conf, validate);
        }

        [TestMethod]
        public void CheckBasic()
        {
            var conf = new BasicConfiguration {OutputDirectory = "Basic"};
            conf.Serialize(BasicFilePath);
            
            var validate = new BasicConfiguration();
            validate.Deserialize(BasicFilePath);

            Check(conf, validate);
        }

        [TestMethod]
        public void CheckImportantIssues()
        {
            var conf = new ImportantIssuesConfiguration {OutputDirectory = "Important"};
            conf.Serialize(ImportantIssuesFilePath);

            var validate = new ImportantIssuesConfiguration();
            validate.Deserialize(ImportantIssuesFilePath);

            Check(conf, validate);
        }
    }
}