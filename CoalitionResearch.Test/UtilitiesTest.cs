﻿using System;
using CoalitionResearch.Core.Profiles;
using CoalitionResearch.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoalitionResearch.Test
{
    [TestClass]
    public class UtilitiesTest
    {
        [TestMethod]
        public void TestPrototypes()
        {
            var header = new ProfileHeader(6);
            var prototypes = new Prototypes(header, 2);

            foreach (var profile in prototypes.Profiles)
                Console.WriteLine(profile);
        }

        [TestMethod]
        public void TestRand()
        {
            var image = new GroupImages(10);

            image.Image1[0] = 1;
            image.Image1[1] = 1;
            image.Image1[2] = 1;
            image.Image1[3] = 2;
            image.Image1[4] = 1;
            image.Image1[5] = 3;
            image.Image1[6] = 2;
            image.Image1[7] = 4;
            image.Image1[8] = 1;
            image.Image1[9] = 2;

            image.Image2[0] = 2;
            image.Image2[1] = 1;
            image.Image2[2] = 2;
            image.Image2[3] = 4;
            image.Image2[4] = 5;
            image.Image2[5] = 3;
            image.Image2[6] = 2;
            image.Image2[7] = 2;
            image.Image2[8] = 1;
            image.Image2[9] = 1;

            Console.WriteLine($"Int: {image.IntRandMeasure}, Total: {image.RandMeasureSize}");
        }

        [TestMethod]
        public void TestRandNone()
        {
            var image = new GroupImages(10);

            image.Image1[0] = 1;
            image.Image1[1] = 1;
            image.Image1[2] = 1;
            image.Image1[3] = 1;
            image.Image1[4] = 1;
            image.Image1[5] = 1;
            image.Image1[6] = 1;
            image.Image1[7] = 1;
            image.Image1[8] = 1;
            image.Image1[9] = 1;

            image.Image2[0] = 1;
            image.Image2[1] = 2;
            image.Image2[2] = 3;
            image.Image2[3] = 4;
            image.Image2[4] = 5;
            image.Image2[5] = 6;
            image.Image2[6] = 7;
            image.Image2[7] = 8;
            image.Image2[8] = 9;
            image.Image2[9] = 10;

            Assert.AreEqual(0, image.IntRandMeasure);

            image.Image2[0] = 2;
            image.Image2[1] = 2;
            image.Image2[2] = 2;
            image.Image2[3] = 2;
            image.Image2[4] = 2;
            image.Image2[5] = 2;
            image.Image2[6] = 2;
            image.Image2[7] = 2;
            image.Image2[8] = 2;
            image.Image2[9] = 2;

            Assert.AreEqual(45, image.IntRandMeasure);
        }
    }
}
