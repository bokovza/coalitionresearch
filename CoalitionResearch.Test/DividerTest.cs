﻿using CoalitionResearch.Core;
using CoalitionResearch.Core.Profiles;
using CoalitionResearch.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace CoalitionResearch.Test
{
    [TestClass]
    public class DividerTest
    {
        [TestMethod]
        public void CheckDivider()
        {
            var header = new ProfileHeader(100);
            var context = new VotingContext(header, 1100, 11)
            {
                ElectoralThreshold = 0.05
            };

            var proto = new Prototypes(header, 10, new NoiseFilter(0));

            for (var i = 0; i < proto.Profiles.Length; i++)
            {
                var profile = proto.Profiles[i];

                for(var vIndex = i * 100; vIndex < (i+1)*100; vIndex++)
                {
                    var voter = context.Voters[vIndex];
                    voter.Profile.Copy(profile);

                    if(vIndex == i*100)
                    {
                        context.Parties.ElementAt(i).SetFirstVoter(voter);
                    }
                }
            }

            context.Stablize();
            Assert.AreEqual(11, context.Parties.Count());
        }
    }
}
