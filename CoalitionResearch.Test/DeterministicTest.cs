﻿using CoalitionResearch.Core.Profiles;
using CoalitionResearch.Core;
using System.Collections.Generic;
using System.Linq;
using CoalitionResearch.Utilities;
using System;
//using static Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static System.Console;
using CoalitionResearch.Records;

namespace CoalitionResearch.Test
{
    [TestClass]
    public class DeterministicTest
    {
        #region Members
        private VotingContext _context;
        #endregion

        #region Helpers
        private void SetVoters(ICollection<Tuple<int, int[]>> options)
        {
            var i = 0;

            foreach(var tuple in options)
            {
                var count = 1;
                for (; i < _context.Voters.Length && count <= tuple.Item1; i++, count++)
                    _context.Voters[i].Profile.Copy(tuple.Item2);
            }
        }

        private void SetParties(ICollection<int[]> options)
        {
            for (var i = 0; i < options.Count; i++)
                _context.Parties.ElementAt(i)?.Profile.Copy(options.ElementAt(i));
        }

        private static int[] CreateImage(params Tuple<int, int>[] numbers)
        {
            var result = new int[numbers.Select(num => num.Item1).Sum()];

            for (int i = 0, index = 0; i < numbers.Length; i++)
                for (var j = 0; j < numbers[i].Item1; j++, index++)
                    result[index] = numbers[i].Item2;

            return result;
        }
        
        private static string PrintList<T>(ICollection<T> list)
        {
            if (list == null || list.Count == 0)
                return "";

            var result = "";
            for (var i = 0; i < list.Count; i++)
                result += (i > 0 ? "," : "") + list.ElementAt(i);
            return result;
        } 

        private static void Copy<T>(T[] source, T[] destination)
        {
            var min = Math.Min(source?.Length ?? 0, destination?.Length ?? 0);

            for (var i = 0; i < min; i++)
                destination[i] = source[i];
        }

        private int[] CreateImage()
        {
            var size = _context.Voters.Length;
            var result = new int[size];

            for (var i = 0; i < size; i++)
                result[i] = _context.Voters[i].Party.Id;

            return result;
        }

        private void PrintCoalition(Coalition c)
        {
            if (c == null)
                return;
            
            WriteLine($"Coalition: {c}");
            WriteLine($"Profile: {c.Profile}");
            WriteLine($"Resulted Profile: {c.ResultedProfile}");
            WriteLine($"Size: {c.Size}");
            WriteLine("------------------------------------");
            WriteLine($"Values:");
            foreach (var u in c.Values)
                WriteLine($"{u.Key.Id} => {u.Value}");
            WriteLine("====================================");
        }
        #endregion

        public void Initialize2()
        {
            _context = new VotingContext(new ProfileHeader(3), 50, 2);

            SetVoters(
               new List<Tuple<int, int[]>>
               {
                    new Tuple<int, int[]>(40, new int[] {1, 1, 1}),
                    new Tuple<int, int[]>(10, new int[] {-1, -1, -1})
               });

            SetParties(new List<int[]>
            {
                new int[] {-1, -1, -1},
                new int[] {1, 1, 1}
            });

            _context.ElectoralThreshold = 0.05;
        }

        [TestMethod]
        public void TestRadical()
        {
            _context.Stablize();
            int a;
            bool b;
            var coalition = _context.FormatCoalition(out a, out b);

            var sum = 0.0;
            foreach (var party in coalition.Parties)
                sum += coalition.Values[party];

            var single = _context.SinglePartyValue;

            WriteLine($"Single Party : {single}");
            WriteLine($"Coalition: {sum}");
        }

        [TestInitialize]
        public void Initialize()
        {
            _context = new VotingContext(new ProfileHeader(4), 60, 6);

            SetVoters(
               new List<Tuple<int, int[]>>
               {
                    new Tuple<int, int[]>(2, new int[] {1, 1, 1, 1}),
                    new Tuple<int, int[]>(5, new int[] {1, 1, 1, -1}),
                    new Tuple<int, int[]>(4, new int[] {1, 1, -1, 1}),
                    new Tuple<int, int[]>(4, new int[] {1, 1, -1, -1}),

                    new Tuple<int, int[]>(8, new int[] {1, -1, 1, 1}),
                    new Tuple<int, int[]>(2, new int[] {1, -1, 1, -1}),
                    new Tuple<int, int[]>(1, new int[] {1, -1, -1, 1}),
                    new Tuple<int, int[]>(5, new int[] {1, -1, -1, -1}),

                    new Tuple<int, int[]>(3, new int[] {-1, 1, 1, 1}),
                    new Tuple<int, int[]>(5, new int[] {-1, 1, 1, -1}),
                    new Tuple<int, int[]>(2, new int[] {-1, 1, -1, 1}),
                    new Tuple<int, int[]>(4, new int[] {-1, 1, -1, -1}),

                    new Tuple<int, int[]>(2, new int[] {-1, -1, 1, 1}),
                    new Tuple<int, int[]>(7, new int[] {-1, -1, 1, -1}),
                    new Tuple<int, int[]>(2, new int[] {-1, -1, -1, 1}),
                    new Tuple<int, int[]>(4, new int[] {-1, -1, -1, -1})
               }
            );

            SetParties(new List<int[]>
            {
                new int[] {1, -1, 1, -1},
                new int[] {-1, 1, 1, 1},
                new int[] {-1, -1, -1, 1},
                new int[] {-1, -1, 1, 1},
                new int[] {1, 1, -1, -1},
                new int[] {1, 1, -1, 1}
            });

            _context.ElectoralThreshold = 0.1;
        }

        [TestMethod]
        public void RunDeterministicTest()
        {
            var record = new RunRecord();
            _context.Stablize();
            _context.GenerateMajor();
            record.FormatCoalition(_context);

            WriteLine($"CR: {record.CoalitionValue}");
            WriteLine($"OR: {record.OppositionValue}");
            WriteLine("------------------------------------");
            WriteLine($"SP: {record.SinglePartyValue}");
            WriteLine($"CSP:{record.CoalitionSinglePartyValue}");
            WriteLine($"OSP:{record.OppositionSinglePartyValue}");
            WriteLine($"TR:{record.TotalValue}");
            
            //WriteLine("Proper Coalitions");
            //WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            //foreach (var coalition in _context.ProperCoalitions)
            //    PrintCoalition(coalition);
            //WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            //WriteLine("Core Coalitions");
            //WriteLine("######################################");
            //foreach (var coalition in _context.CoreCoalitions)
            //    PrintCoalition(coalition);
            //WriteLine("######################################");
            //WriteLine($"Chosen Coalition: {chosen}");

        }

        [TestMethod]
        public void TestRandMeasure()
        {
            var a = CreateImage(
                new Tuple<int, int>(15, 1),
                new Tuple<int, int>(16, 2),
                new Tuple<int, int>(14, 3),
                new Tuple<int, int>(15, 4)
                );

            var b = CreateImage(
                new Tuple<int, int>(2, 2),
                new Tuple<int, int>(5, 1),
                new Tuple<int, int>(8, 5),
                new Tuple<int, int>(10, 1),
                new Tuple<int, int>(1, 5),
                new Tuple<int, int>(5, 1),
                new Tuple<int, int>(10, 2),
                new Tuple<int, int>(4, 3),
                new Tuple<int, int>(2, 2),
                new Tuple<int, int>(7, 1),
                new Tuple<int, int>(6, 3));

            var images = new GroupImages(60);
            Copy(a, images.Image1);
            Copy(b, images.Image2);

            _context.Stablize();

            var b2 = CreateImage();

            var images2 = new GroupImages(60);
            Copy(a, images2.Image1);
            Copy(b2, images2.Image2);

            WriteLine($"Before : {PrintList(images.Image1)}");
            WriteLine($"After : {PrintList(images.Image2)}");
            WriteLine($"========================================");
            WriteLine($"After : {PrintList(images2.Image2)}");

            WriteLine(images2.IntRandMeasure);
            WriteLine(images2.RandMeasureSize);
            WriteLine(images2.RandMeasure);
        }
    }
}