﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace CoalitionResearch.Test
{
    [TestClass]
    public class MakeRand
    {
        [TestMethod] 
        public void AddRand()
        {
            using (var aggregation = new StreamReader("Basic/aggregation.csv"))
                using (var result = new StreamWriter("Basic/aggregation_fixed.csv"))
            {
                result.WriteLine(aggregation.ReadLine() + ",AverageRandMeasure");

                while(!aggregation.EndOfStream)
                {
                    var line = aggregation.ReadLine();
                    var splitted = line.Split(',');

                    using (var file = new StreamReader($"Basic/i100_p15_v1000_a{splitted[0]}_b{splitted[1]}_pr0.05_np{splitted[6]}_nv{splitted[7]}.csv"))
                    {
                        double sum = 0;
                        file.ReadLine();

                        while(!file.EndOfStream)
                        {
                            var tmpLine = file.ReadLine();
                            var tmpSplit = tmpLine.Split(',');
                            sum += double.Parse(tmpSplit[3]);
                        }

                        result.WriteLine(line + "," + sum / 100);
                    }
                }
            }
        }
    }
}
