﻿using CoalitionResearch.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static CoalitionResearch.Records.RecordService;

namespace CoalitionResearch.Test
{
    [TestClass]
    public class ImagesTest
    {
        [TestMethod]
        public void CheckRandMeasure()
        {
            var images = new GroupImages(4);

            images.Image1[0] = 1;
            images.Image1[1] = 1;
            images.Image1[2] = 0;
            images.Image1[3] = 2;

            images.Image2[0] = 3;
            images.Image2[1] = 1;
            images.Image2[2] = 1;
            images.Image2[3] = 2;

            Assert.AreEqual(6, images.RandMeasureSize);
            Assert.AreEqual(4, images.IntRandMeasure);
        }

        [TestMethod]
        public void CheckName()
        {
            Assert.AreEqual("Check It Out", PrintName("CheckItOut"));
        }
    }
}
