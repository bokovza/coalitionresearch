﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CoalitionResearch.Core;
using CoalitionResearch.Core.Profiles;
using MathNet.Numerics.Distributions;
using CoalitionResearch.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace CoalitionResearch.Test
{
    [TestClass]
    public class RandMeasureTest
    {
        #region Members
        private VotingContext ctx;
        private ProfileHeader header;
        #endregion

        [TestMethod]
        public void ExecuteTest()
        {
            for (var k = 3; k <= 10; k++)
                Print(k, k);
        }

        
        public double CreateAndRun(int parties, int groups)
        {
            header = new ProfileHeader(100);
            ctx = new VotingContext(header, 1000, parties);

            foreach (var voter in ctx.Voters)
                for (var i = 0; i < header.Size; i++)
                    voter[i] = 2 * DiscreteUniform.Sample(0, 1) - 1;

            var img = new GroupImages(ctx.Voters.Length);
            for (var i = 0; i < img.Image1.Length; i++)
                img.Image1[i] = DiscreteUniform.Sample(1, groups);

            foreach(var party in ctx.Parties)
                for(var i = 0; i < header.Size; i++)
                    party[i] = 2 * DiscreteUniform.Sample(0, 1) - 1;

            var iterations = ctx.Stablize();

            {
                var i = 0; 

                foreach(var voter in ctx.Voters)
                {
                    img.Image2[i] = voter.Party.Id;
                    i++;
                }
            }

            return img.RandMeasure;
        }

        public void Print(int parties, int groups)
        {
            System.Console.WriteLine($"Parties: {parties}; Groups {groups}");
            System.Console.WriteLine("------------------------------------------");

            for(var i = 1; i <= 100; i++)
                System.Console.WriteLine(CreateAndRun(parties, groups));

            System.Console.WriteLine("==========================================");
        }

        [TestMethod]
        public void CheckNaN()
        {
            var lst = new List<double> { 12.3456, -7.3316, double.NaN, 7.6498 };
            var lst2 = new List<double> { 12.3456, -7.3316, double.NaN, 7.6498, double.NegativeInfinity };

            var a = lst.Where(item => !double.IsNaN(item)).ToList();
            var b = lst.Where(item => item != double.NaN).ToList();
        }
    }
}
