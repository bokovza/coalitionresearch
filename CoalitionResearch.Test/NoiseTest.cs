﻿using CoalitionResearch.Utilities;
using MathNet.Numerics.Distributions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using static System.Console;

namespace CoalitionResearch.Test
{
    [TestClass]
    public class NoiseTest
    {
        [TestMethod]
        public void TestNoise()
        {
            for (var k = 1; k <= 1000; k++)
            {
                var noise = new NoiseFilter(5, 10);

                var count1 = 0;
                var countMinus1 = 0;

                for (var i = 1; i <= 100000; i++)
                {
                    var result = noise.Generate(1);
                    if (result == 1)
                        count1++;
                    else
                        countMinus1++;
                }

                WriteLine($"{count1},{countMinus1}");
            }
        }

        [TestMethod]
        public void TestRand()
        {
            var array = new int[1000];
            var array2 = new int[1000];

            using(var file = new StreamWriter("testRand.csv"))
            {
                file.WriteLine("rand,uniform");

                var rand = new Random();
                var rand2 = new DiscreteUniform(0, 999);

                for (var i = 1; i <= 1000000; i++)
                {
                    array[rand.Next(1000)]++;
                    array2[rand2.Sample()]++;
                }

                for (var i = 0; i < array.Length; i++)
                    file.WriteLine(array[i] + "," + array2[i]);
            }
        }
    }
}
