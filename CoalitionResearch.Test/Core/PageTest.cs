﻿using System.Collections.Generic;
using System.Linq;
using CoalitionResearch.Core;
using CoalitionResearch.Core.Profiles;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static System.Console;

namespace CoalitionResearch.Test.Core
{
    [TestClass]
    public class PageTest
    {
        private void SetProfileForVoter(VotingContext ctx, int from, int amount, params int[] profile)
        {
            for (var i = from; i < from + amount; i++)
            {
                var voter = ctx.Voters.ElementAt(i);
                for (var k = 0; k < profile.Length; k++)
                    voter[k] = profile[k];
            }
        }
        private void SetProfileForParty(VotingContext ctx, int id, params int[] profile)
        {
            var voter = ctx.Parties.ElementAt(id);
            for (var k = 0; k < profile.Length; k++)
                voter[k] = profile[k];
        }
        [TestMethod]
        public void Run()
        {
            var ctx = new VotingContext(new ProfileHeader(3), 5, 3);

            SetProfileForVoter(ctx, 0, 1, -1, -1, -1);
            SetProfileForVoter(ctx, 1, 1, 1, 1, 1);
            SetProfileForVoter(ctx, 2, 1, -1, 1, -1);
            SetProfileForVoter(ctx, 3, 1, 1, 1, -1);
            SetProfileForVoter(ctx, 4, 1, -1, 1, 1);

            SetProfileForParty(ctx, 0, 1 ,1, 1);
            SetProfileForParty(ctx, 1, -1, 1, -1);
            SetProfileForParty(ctx, 2, -1, -1, -1);

            ctx.ElectoralThreshold = 0.05;
            ctx.Stablize();

            var count = ctx.Parties.Count;
            for (var i = 0; i < count; i++)
            {
                var party = ctx.Parties.ElementAt(i);
                WriteLine($"|p{party.Id}|={party.Size}");
            }
        }
        
        private void SetVoters(VotingContext context, int startIndex, int size, int partyId, params int[] profile)
        {
            var party = context.Parties.Where(item => item.Id == partyId).ElementAt(0);

            for (var i = startIndex; i < startIndex + size; i++)
            {
                var voter = context.Voters.ElementAt(i);
                for (var k = 0; k < profile.Length; k++)
                    voter[k] = profile[k];
                
                voter.Party = party;
            }
        }


        [TestMethod]
        public void CheckUtility()
        {
            var context = new VotingContext(new ProfileHeader(3), 21, 3);

            SetProfileForParty(context, 0, 1, 1, 1);
            SetProfileForParty(context, 1, -1, 1, -1);
            SetProfileForParty(context, 2, -1, -1, -1);

            SetVoters(context, 0, 6, 3, -1, -1, -1);
            SetVoters(context, 6, 3, 1, 1, 1, 1);
            SetVoters(context, 9, 3, 2, -1, 1, -1);
            SetVoters(context, 12, 4, 1, 1, 1, -1);
            SetVoters(context, 16, 5, 2, -1, 1, 1);

            var count = context.Parties.Count;
            var dict = new Dictionary<int, Party>();
            for (var i = 0; i < count; i++)
            {
                var party = context.Parties.ElementAt(i);
                dict.Add(party.Id, party);
                party.Stablize();
                WriteLine($"|p{party.Id}|={party.Size}");
            }

            var c1 = new Coalition(context, new List<Party> { dict[1], dict[2] });
            var c2 = new Coalition(context, new List<Party> { dict[2], dict[3] });
            var c3 = new Coalition(context, new List<Party> { dict[1], dict[3] });

            Assert.AreEqual(-1, c1.Values[dict[1]]);
            Assert.AreEqual(18, c1.Values[dict[2]]);
            Assert.AreEqual(-6, c1.Values[dict[3]]);

            Assert.AreEqual(1, c2.Values[dict[1]]);
            Assert.AreEqual(14, c2.Values[dict[2]]);
            Assert.AreEqual(6, c2.Values[dict[3]]);

            Assert.AreEqual(15, c3.Values[dict[1]]);
            Assert.AreEqual(-2, c3.Values[dict[2]]);
            Assert.AreEqual(-6, c3.Values[dict[3]]);
        }
    }
}