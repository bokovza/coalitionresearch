# Coalition Research - README #

Author: [Mr. Bar Yochai Bokovza](barboko@post.bgu.ac.il)

Lecturer: [Dr. Meir Kalech](kalech@bgu.ac.il)

This project is a part of a course called "Research Skills" in the Engineering Faculty of Ben-Gurion University of the Negev, and programmed for Dr. Meir Kalech from the Dept. of Infomation Systems Engineering.

## Run Executables ##

In the **Executables** Folder, there are 2 *.exe files:
* Utility.exe
* Regret.exe

Each execution file does the same, but uses different formulas (the full list of formulas are in the **Atricles** Folder, in the **Final Report (Hebrew).pdf** file.

### Instructions ###
Write the demanded command line:

* For running **Utility.exe** in the **Basic Mode** : Utility.exe basic utilityBasic.txt
* For running **Utility.exe** in the **Important Issues Mode** : Utility.exe importance utilityImportance.txt

* For running **Regret.exe** in the **Basic Mode** : Regret.exe basic regretBasic.txt
* For running **Regret.exe** in the **Important Issues Mode** : Regret.exe importance regretImportance.txt

## Configuration Files ##
These are files that saves the parameters for running the executables.

### Basic Mode ###

* Issues : How many issues are avaliable in each profile ? (=Profile's vector size)
* Runs: How many runs the code will run with the same parameters ?
* Voters : How many voters there are in each election system ?
* Parties : How many parties will be in each election system at the beginning ?
* ElectoralThreshold: How many precents from the voters, each party should hold, without erase it ?
* OutputDirectory: The name of the directory, which the output files will be hosted.
* PrototypeJump: The amount of issues that changes with every new prototype for the voters.
* BetaValues: List of <alpha,beta> divided by | sign.
* BaseNoisePrototype & BaseNoiseVoters: The denomerator for the Noise Filters.
* NoisePrototypes & NoiseVoters: List of numerators for the Noise Filters.

### Important Issues Mode ###
**All Properties from Basic Mode must be applied.**

* ImportantIssues: List of amounts of issues that will be important for each voter.

## Compilation Flags ##
1. REGRET - Use this flag if you want to use the Regret Formulas. If you're not using that flag, the code will be using the Utility Formulas.
2. DETERMINISTIC - Use this flag only for tests (it makes the tie-breaker rule in Algorithm 1 from non-deterministic, to a deterministic rule)