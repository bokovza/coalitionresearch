﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CoalitionResearch.Core.Profiles;
using System.Linq;
using CoalitionResearch.Core.Utilities;

namespace CoalitionResearch.Core
{
    /// <summary>
    /// The Context that contain all the voters and parties in the world
    /// </summary>
    public class VotingContext
    {
        #region Members
        /// <summary>
        /// The dictionary that holds all the parties in the context
        /// </summary>
        private readonly IList<Party> _parties;

        /// <summary>
        /// The dictionary that holds all the voters in the context
        /// </summary>
        private readonly IList<Voter> _voters;
        #endregion

        #region Properties
        /// <summary>
        /// The Header of the Profile
        /// </summary>
        public ProfileHeader Header { get; }
        /// <summary>
        /// Dictionary of parties
        /// </summary>
        public IReadOnlyCollection<Party> Parties { get; }
        /// <summary>
        /// Dictionary of voters
        /// </summary>
        public IReadOnlyCollection<Voter> Voters { get; }
        /// <summary>
        /// Counter to give Id numbers for the parties
        /// </summary>
        public Counter CounterParties { get; } = new Counter();
        /// <summary>
        /// Counter to give Id numbers for the voters
        /// </summary>
        public Counter CounterVoters { get; } = new Counter();
        /// <summary>
        /// The Electoral Threshold (precentages)
        /// </summary>
        public double ElectoralThreshold { get; set; }
        /// <summary>
        /// Number of voters to reach Electoral Threshold
        /// </summary>
        public int ElectoralThresholdVotersSize => (int)(Math.Round(ElectoralThreshold * Voters.Count)) + 1;

        /// <summary>
        /// Number of voters to reach Majority of voters
        /// </summary>
        public int MajorityVotersSize => (int)Math.Round(0.5 * Voters.Count) + 1;

        /// <summary>
        /// List of Proper Coalitions
        /// </summary>
        public ICollection<Coalition> ProperCoalitions { get; }
        /// <summary>
        /// List of Coalitions in the core
        /// </summary>
        public ICollection<Coalition> CoreCoalitions { get; }
        /// <summary>
        /// Amount of voters in the context
        /// </summary>
        private int VotersCount { get; }
        /// <summary>
        /// Amount of parties in the context
        /// </summary>
        private int PartiesCount { get; }

        #endregion

        #region Constructors

        /// <summary>
        /// Basic Constructor
        /// </summary>
        /// <param name="header">Profile's Header</param>
        /// <param name="voters">Amount of voters at startup</param>
        /// <param name="parties">Amount of parties at startup</param>
        public VotingContext(ProfileHeader header, int voters, int parties)
        {
            if ((Header = header) == null)
                throw new ArgumentNullException(nameof(header));
            if((VotersCount = voters) <= 0)
                throw new ArgumentOutOfRangeException(nameof(voters), "Must be above 0.");
            if ((PartiesCount = parties) <= 0)
                throw new ArgumentOutOfRangeException(nameof(parties), "Must be above 0.");

            _parties = new List<Party>(PartiesCount);
            _voters = new List<Voter>(VotersCount);

            Parties = new ReadOnlyCollection<Party>(_parties);
            Voters = new ReadOnlyCollection<Voter>(_voters);

            ProperCoalitions = new List<Coalition>();
            CoreCoalitions = new List<Coalition>();

            Build();
            Reset();
        }

        /// <summary>
        /// Build the voters of the context.
        /// </summary>
        private void Build()
        {
            for(var i = 1; i <= VotersCount; i++)
                _voters.Add(new Voter(this, i));
        }

        /// <summary>
        /// Reset the context from scratch.
        /// </summary>
        public void Reset()
        {
            foreach (var voter in Voters)
                voter.Party = null;

            _parties.Clear();
            for (var i = 1; i <= PartiesCount; i++)
                _parties.Add(new Party(this, i));
        }
        #endregion

        #region Methods
        /// <summary>
        /// This method stablize the parties, and remove parties with voters under the electoral threshold.
        /// </summary>
        /// <returns>Amount of iterations</returns>
        /// TODO: Fix This & Add Documentation
        public int Stablize()
        {
            var parties = new List<Party>();
            parties.AddRange(Parties);

            var iterations = 0;

            var isChanged = true;
            while(isChanged)
            {
                var k = Voters.Select(voter => voter.FindParty(parties)).Where(item => item).Count();
                var emptyParties = parties.Where(party => party.Voters.Count < ElectoralThresholdVotersSize).ToList();
                isChanged = emptyParties.Count > 0 || k > 0;
                parties = parties.Except(emptyParties).ToList();
                
                foreach (var party in emptyParties)
                {
                    var emptyVoters = party.Voters.Values.ToList();

                    foreach (var voter in emptyVoters)
                        voter.FindParty(parties);
                    _parties.Remove(party);
                }

                parties.ForEach(party => party.Stablize());
                iterations++;
            }

            return iterations;
        }

        /// <summary>
        /// Creates proper coalitions and put it in the ProperCoalitions' List
        /// </summary>
        private void GenerateProperCoalitions()
        {
            ProperCoalitions.Clear();

            var parties = Parties.ToList();

            var incrementor = new Incrementor(parties.Count);

            while (!incrementor.Overflow)
            {
                var coalitionParties = new HashSet<Party>();
                for (var i = 0; i < parties.Count; i++)
                    if (incrementor[i])
                        coalitionParties.Add(parties.ElementAt(i));

                var coalition = new Coalition(this, coalitionParties);
                if (coalition.IsProper)
                    ProperCoalitions.Add(coalition);

                incrementor.Increment();
            }
        }

        /// <summary>
        /// Creates coalitions of the core and put it in the CoreCoalitions' List
        /// </summary>
        private void GenerateCoreCoalitions()
        {
            GenerateProperCoalitions();

            CoreCoalitions.Clear();
            if (ProperCoalitions.Count <= 1)
            {
                if(ProperCoalitions.Count == 1)
                    CoreCoalitions.Add(ProperCoalitions.ElementAt(0));
                return;
            }
            
            foreach(var coalition in ProperCoalitions)
            {
                if (coalition == null)
                    continue;

                var inCore = false;
                
                foreach(var otherCoalition in ProperCoalitions)
                {
                    if (otherCoalition == coalition) continue;

                    if (coalition.Parties.Any(party => otherCoalition.Utilities[party] <= coalition.Utilities[party]))
                    {
                        inCore = true;
                        break;
                    }
                }

                if (inCore)
                    CoreCoalitions.Add(coalition);
            }
        }

        /// <summary>
        /// Finds the best coalition, using priority based on parties size
        /// </summary>
        /// <returns>The best coalition</returns>
        public Coalition FormatCoalition(out int leaderIndex, out bool isMinimumSubset)
        {
            var parties = Parties.OrderByDescending(party => party.Size).Select(item => item).ToList();
            GenerateCoreCoalitions();

            leaderIndex = -1;
            var count = 0;

            foreach (var party in parties)
            {
                var core = CoreCoalitions.Where(coalition => coalition.Parties.Contains(party)).ToList();
                var minSize = core.Min(coalition => coalition.Size);

                if (core.Count == 0)
                {
                    count++;
                    continue;
                }

                var maxCoalition = core.ElementAt(0);
                var maxUtility = maxCoalition.Utilities[party];

                for (var i = 1; i < core.Count; i++)
                {
                    var coalition = core.ElementAt(i);
                    var utility = coalition.Utilities[party];

                    if (maxUtility >= utility) continue;

                    maxCoalition = coalition;
                    maxUtility = utility;
                }

                leaderIndex = count;
                isMinimumSubset = minSize == maxCoalition.Size;
                return maxCoalition;
            }

            isMinimumSubset = false;
            return null;
        }

        /// <summary>
        /// Measures the utility of the "single party"
        /// </summary>
        /// <returns>Utility of the "single party"</returns>
        public int CalculateSinglePartyUtility()
        {
            var sum = 0;
            for (var i = 0; i < Header.Size; i++)
                sum += Math.Abs(Voters.Sum(voter => voter.Profile[i]));

            return sum;
        }
        #endregion
    }
}