﻿using System;
using CoalitionResearch.Core.Utilities;

namespace CoalitionResearch.Core.Profiles
{
    /// <summary>
    /// An Array of values that each value presents the opinion about a specific issue.
    /// </summary>
    public class Profile
    {
        #region Properties
        /// <summary>
        /// The scheme for the profile
        /// </summary>
        public ProfileHeader Header { get; }
        /// <summary>
        /// Shortcut for Header.Size property
        /// </summary>
        public int Size => Header.Size;
        /// <summary>
        /// The values of the profile
        /// </summary>
        private int[] Data { get; }
        /// <summary>
        /// Shortcut for the data in the profile.
        /// Setter can get only -1 and 1.
        /// </summary>
        /// <param name="index">The index</param>
        /// <returns>-1 or 1, depends what stored in Data[i]</returns>
        public int this[int index]
        {
            get { return Data[index]; }
            set
            {
                if (value != Data[index] && (value == -1 || value == 1))
                    Data[index] = value;
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Base structure
        /// </summary>
        /// <param name="header">The ProfileHeader of the profile</param>
        public Profile(ProfileHeader header)
        {
            if ((Header = header) == null)
                throw new ArgumentNullException(nameof(Header), "Header can't be null.");

            Data = new int[Header.Size];
            for (var i = 0; i < Data.Length; i++)
                Data[i] = -1;
        }
        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="p"></param>
        public Profile(Profile p)
        {
            if(p == null)
                throw new ArgumentNullException(nameof(p), "Copy Constructor didn't get an object.");

            Header = p.Header;
            Data = new int[Header.Size];
            for (var i = 0; i < Header.Size; i++)
                this[i] = p[i];
        }
        #endregion

        #region Methods
        /// <summary>
        /// Function that calculates the Hamming Distance between this profile and p.
        /// </summary>
        /// <param name="p">The other profile to calculate the distance to that.</param>
        /// <returns>Hamming Distance</returns>
        public int HammingDistance(Profile p)
        {
            if (p == null || !Equals(p.Header, Header))
                throw new ArgumentNullException(nameof(p));

            var result = 0;

            for (var i = 0; i < Size; i++)
                if (p[i] != this[i])
                    result++;

            return result;
        }

        /// <summary>
        /// Copy Method of the profile
        /// </summary>
        /// <param name="p">The profile we copy from</param>
        /// <returns><b>True</b> if the copy succeed</returns>
        public virtual bool Copy(Profile p)
        {
            var result = p != null && Header.Equals(p.Header);

            // ReSharper disable once InvertIf
            if (result)
                for (var i = 0; i < Header.Size; i++)
                    this[i] = p[i];

            return result;
        }

        /// <summary>
        /// ToString of the profile
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"[{StringFunctions.CreateListString(Data, ';')}]";
        #endregion
    }
}