﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CoalitionResearch.Core.Profiles;
using CoalitionResearch.Core.Utilities;

namespace CoalitionResearch.Core
{
    /// <summary>
    /// A set of parties, which combines as a "single party"
    /// </summary>
    public class Coalition
    {
        #region Properties
        /// <summary>
        /// The Context of the Coalition
        /// </summary>
        public VotingContext Context { get; }
        /// <summary>
        /// The voters from all the parties, which are in the coalition
        /// </summary>
        public ICollection<Voter> Voters { get; }
        /// <summary>
        /// The parties in the coalition
        /// </summary>
        public ICollection<Party> Parties { get; }
        /// <summary>
        /// Coalition's Profile
        /// </summary>
        public CoalitionProfile Profile { get; }
        /// <summary>
        /// The resulted profile, given the coalition
        /// </summary>
        public Profile ResultedProfile { get; }

        /// <summary>
        /// Amount of voters in the coalition
        /// </summary>
        public int Size { get; }
        /// <summary>
        /// Returns true if the amount of voters in the coalition is bigger than 50% of all voters in the context.
        /// </summary>
        public bool IsProper => Voters.Count > Context.MajorityVotersSize;
        /// <summary>
        /// Shortcut for the value of an issue in the coalition's profile.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public int this[int index] => Profile[index];

        /// <summary>
        /// Dictionary with the utilities of each party in the context
        /// </summary>
        private readonly IDictionary<Party, int> _utilities = new Dictionary<Party, int>();
        /// <summary>
        /// Public dictionary with the utilities of each party in the context - no editable.
        /// </summary>
        public IReadOnlyDictionary<Party, int> Utilities { get; }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context">The context of the coalition</param>
        /// <param name="parties">The list of parties that are in the coalition</param>
        public Coalition(VotingContext context, ICollection<Party> parties = null)
        {
            if ((Context = context) == null)
                throw new ArgumentNullException(nameof(Context));

            Parties = parties ?? new Party[0];
            Size = Parties.Sum(party => party.Voters.Count);
            Voters = Parties.SelectMany(party => party.Voters.Values).ToList();
            Profile = new CoalitionProfile(this);
            
            ResultedProfile = CalculateResultedProfile();

            Utilities = new ReadOnlyDictionary<Party, int>(_utilities);
            CalculateUtilities();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Calculation of the Resulted Profile
        /// </summary>
        /// <returns>Resulted Profile</returns>
        private Profile CalculateResultedProfile()
        {
            var result = new Profile(Context.Header);

            var lst = new List<PowerProfile>() {Profile};
            lst.AddRange(Context.Parties.Except(Parties).Select(party => party.Profile).ToList());

            for (var i = 0; i < Context.Header.Size; i++)
                result[i] = lst.Sum(profile => profile[i]*profile.Power[i]) > 0 ? 1 : -1;

            return result;
        }

        /// <summary>
        /// Calculation of the utilities of all parties in the context.
        /// </summary>
        private void CalculateUtilities()
        {
            foreach (var party in Context.Parties)
            {
                var sum = 0;
                for (var i = 0; i < Context.Header.Size; i++)
                    sum += ResultedProfile[i] * party[i] * party.Profile.Power[i];

                _utilities.Add(party, sum);
            }
        }

        public override string ToString() => $"[{StringFunctions.CreateListString(Parties.Select(party => party.Id), ' ')}]";
        #endregion
    }
}