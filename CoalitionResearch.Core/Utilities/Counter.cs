﻿namespace CoalitionResearch.Core.Utilities
{
    /// <summary>
    /// Allow create serial numbers for elements, thread-safe.
    /// </summary>
    public class Counter
    {
        #region Members
        /// <summary>
        /// The actual value that is in the counter.
        /// </summary>
        private int _value;
        #endregion

        #region Locks
        /// <summary>
        /// Lock for keep consistency between threads, just in case.
        /// </summary>
        private readonly object _lock = new object();
        #endregion

        #region Properties
        /// <summary>
        /// The actual value that the user is request the value from in the counter.
        /// <strong>Get:</strong> returns the stored value and raise it with 1.
        /// <strong>Set:</strong> reset the counter with a special value.
        /// </summary>
        public int Value
        {
            get
            {
                int result;
                lock (_lock)
                {
                    result = _value;
                    _value++;
                }
                return result;
            }
            
            set
            {
                lock (_lock)
                    _value = value;
            }
        }

        /// <summary>
        /// The value to start from
        /// </summary>
        public int StartingPoint { get; }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor for Counter.
        /// </summary>
        /// <param name="start">From which value the counter will start to count.</param>
        public Counter(int start = 1)
        {
            StartingPoint = start;
            Value = StartingPoint;
        }
        #endregion

        #region Method
        /// <summary>
        /// Reset the counter from the starting point
        /// </summary>
        public void Reset() => Value = StartingPoint;
        #endregion
    }
}