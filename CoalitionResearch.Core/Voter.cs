﻿using System;
using System.Collections.Generic;
using CoalitionResearch.Core.Profiles;
using MathNet.Numerics.Distributions;

namespace CoalitionResearch.Core
{
    /// <summary>
    /// Voter in the voting system.
    /// </summary>
    public class Voter
    {
        #region Members
        private Party _party;
        #endregion

        #region Properties
        /// <summary>
        /// The serial number of the voter.
        /// </summary>
        public int Id { get; }
        /// <summary>
        /// The profile of the Voter.
        /// </summary>
        public Profile Profile { get; }
        /// <summary>
        /// The party that the voter is in.
        /// </summary>
        public Party Party
        {
            get { return _party; }
            set
            {
                if (value == _party) return;

                _party?.Remove(this);
                _party = value;
                _party?.Add(this);
            }
        }

        /// <summary>
        /// Shortcut for the ProfileHeader
        /// </summary>
        public ProfileHeader Header => Context.Header;
        /// <summary>
        /// The Voting Context of the voter
        /// </summary>
        public VotingContext Context { get; }

        /// <summary>
        /// Shortcut for the data of the Profile
        /// </summary>
        /// <param name="index">Index</param>
        /// <returns>Data of the profile [index]</returns>
        public int this[int index]
        {
            get { return Profile[index]; }
            set { Profile[index] = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Basic Constructor
        /// </summary>
        /// <param name="context">The Voting Context</param>
        /// <param name="id">Id of the voter (in case we deserialize)</param>
        public Voter(VotingContext context, int id = -1)
        {
            if ((Context = context) == null)
                throw new ArgumentNullException(nameof(context));
            Id = id < 0 ? context.CounterVoters.Value : id;
            Profile = new Profile(Header);
        }

        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="v">The voter we copy from</param>
        public Voter(Voter v)
        {
            if(v == null)
                throw new ArgumentNullException(nameof(v));

            Context = v.Context;
            Id = v.Id;
            Profile = new Profile(v.Profile);
        }
        #endregion

        #region Methods
        /// <summary>
        /// This function finds for the voter the closest party to be
        /// </summary>
        /// <param name="parties">List of parties that the voter can choose from.</param>
        /// <returns><b>True</b> if the voter has changed it's party.</returns>
        public bool FindParty(ICollection<Party> parties)
        {
            var old = Party;
            
            if (parties == null || parties.Count == 0)
                throw new Exception("There's no parties in the Context.");

            Party chosen = null;
            var chosenDistance = int.MaxValue;
            foreach (var party in parties)
            {
                var distance = Profile.HammingDistance(party.Profile);

                if (chosen == null || chosenDistance > distance || 
                    (chosenDistance == distance && chosen != Party && ContinuousUniform.Sample(0,1) >= 0.5))
                {
                    chosen = party;
                    chosenDistance = distance;
                    
                }
            }

            Party = chosen;
            return old != Party;
        }
        
        /// <summary>
        /// Copy the profile to the voter
        /// </summary>
        /// <param name="p">Profile to copy from</param>
        public void Copy(Profile p) => Profile.Copy(p);
        
#endregion

        public override string ToString() => $"{Id} => {Profile}";
    }
}